/*Class for rendering all drawables on screen */

// Author: Riad Zitouni

#ifndef RENDERER_H
#define RENDERER_H

#include "Drawable.h"
#include "IBO.h"

#include "ObjectManager.h"

namespace ge15{

#define MAX_OBJECTS 100000
#define VERTEX_SIZE sizeof(Vertex)
#define OBJECT_SIZE VERTEX_SIZE * 4
#define BUFFER_SIZE MAX_OBJECTS * OBJECT_SIZE
#define INDEX_BUFFER_SIZE 100000

#define SHADER_POSITION_LOCATION 0 
#define SHADER_NORMAL_LOCATION 1

#define SHADER_UV_LOCATION 2
#define SHADER_TEXID_LOCATION 3

#define SHADER_AMBIENT_LOCATION 4
#define SHADER_DIFFUSE_LOCATION 5
#define SHADER_SPECULAR_LOCATION 6
#define SHADER_SHININESS_LOCATION 7

#define SHADER_IS_SELECTED_LOCATION 8

#define MAX_TEXTURES 32

	class Renderer{

	public:

		Renderer();
		~Renderer();

		void load();
		void add(Drawable* drawable);

		void process();
		void draw();

		void bind();
		void unbind();

	private:
		void init();

		GLuint m_VAO;
		GLuint m_VBO;
		IBO* m_IBO;
		Vertex* m_vertexBuffer;
		
		GLsizei m_indexCount;
		GLsizei m_indexOffset;

		GLuint m_activeTextures[MAX_TEXTURES];

		GLuint m_totalIndices[INDEX_BUFFER_SIZE]; // Indices of all active objects

		std::vector<Drawable*> m_selectedObjects;// All selected objects

		ObjectManager* m_objectManager = NULL;

	};
}

#endif
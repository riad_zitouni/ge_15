// Author: Riad Zitouni

// Windows
#include "Window.h"
#include "MenuWindow.h"

// Cameras
#include "Camera.h"
#include "SceneCamera.h"

// GUI
#include "Button.h"
#include "Panel.h"
#include "Gizmo.h"

// Shaders
#include "RenderShader.h"

// Multithreading
#include <thread>

// Other
#include "Ray.h"
#include "ObjectManager.h"
#include "Input.h"
#include "Timer.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Logger.h"
#include "Renderer.h"

using namespace ge15;

void setDirectoryPaths();
void setOpenGLParameters();
void initShaders();
void initDefaultDrawables();
void updateGizmos();
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void applyMovement();
void checkGUIClicks(double x, double y);
void createCrateStack();
Drawable* addObject(const char* path, const glm::vec3 position, const glm::vec3 scale, const Material material, Label label);
void deleteSelectedObjects();
void checkCollisions();
void updateCamera(double x, double y);
void switchCamera(bool toFpsCamera);
void createGUI();
void intersectRay(double mX, double mY);

// Must initilize window before renderer to setup OpenGL
Window window("Default Window", 1024, 576);
MenuWindow menuWindow;
Renderer renderer;

Camera fpsCamera;
SceneCamera sceneCamera;

Input input;
RenderShader renderShader("render.vs", "render.fs");
Shader guiShader("gui.vs", "gui.fs");
Gizmo gizmo;
Logger* logger = Logger::getInstance();

PointLight* defaultPointLight = NULL;

ObjectManager* objectManager = ObjectManager::getInstance();

// Time taken to draw last frame
double frameTime = 0;

double mouseX, mouseY;

// Mouse info for gizmo movement
double lastX = 0.0;
double lastY = 0.0;
float mouseXIntensity = 0.0;
float mouseYIntensity = 0.0;
bool collision = false;

// Tells whether the left mouse button was pressed or is being pressed
bool isLMBPressed = false;
bool isLeftShiftPressed = false;

// Tells whether the middle mouse button (mouse wheel) is being pressed
bool isMWHPressed = false;

const float rotationSensitivity = 5.0;
const float scaleSensitivity = 2.0;

bool fpsCameraActive = false;

// Tells from which side the z gizmo was hit. If from left, ust same x sensitivity. If from right, invert. 
bool zGizmoHitFromLeft = true;

// Stores the path of the main project directory
const char* mainDirectory = NULL;

// Stores the path of the directory where the light object is located
std::string lightDirectory;

// Stores the path of the directory where the gui components are located
std::string guiDirectory;

// Path of 3d sample objects
std::string sampleDirectory;

// List of all gui elements
std::vector<Drawable*> guis;

/*
The thread (menuThread) controls all the actions happening in the menu window. The function below is the thread function.
*/
void menuThreadFunc(HINSTANCE hInstance){

	// Initialize the menu window
	menuWindow = MenuWindow(hInstance);

	if (!menuWindow.init(hInstance)){
		logger->log("ERROR::MAIN::MENU_THREAD_FUNC: ", "Failed to initilize menu window");
	}
	while (!window.close() && menuWindow.running()){

		menuWindow.checkErrors();

		// Force menu window to repaint
		if (logger->update()){
			InvalidateRect(menuWindow.getHandle(), NULL, TRUE);
			logger->setUpdate(false);
		}
	}

}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){

#if RUN_TESTS
	Window_UT window_test;
	IBO_UT ibo_test;
#endif

	//-------------------------------------------- Set Path of Directories ----------------------------------------
	setDirectoryPaths();

	gizmo = Gizmo(sampleDirectory);

	//-------------------------------------------- OpenGL Window Properties ---------------------------------------------
	setOpenGLParameters();
	
	// ------------------------------------------- Shaders, Lights and Materials----------------------------------------------------
	// Bounding volume shaders for debugging only
	Shader boundingVolumesShader("bounding_volumes.vs", "bounding_volumes.fs");
	
	// Initialize lights and shaders
	
	initShaders();

	// ------------------------------------------- Drawables -------------------------------------------------------------
	initDefaultDrawables();

	// Load all the objects to the renderer for rendering
	renderer.load();

	// ------------------------------------------- GUI ---------------------------------------------
	// Create gui componets (buttons, panels, ......) 
	createGUI();

	//-------------------------------------------- Menu Window Initializaitons ------------------------------------------
	// Start the menu window thread
	std::thread menuThread(menuThreadFunc, hInstance);

	// ------------------------------------------- Main Loop -----------------------------------------------------------------
	// Timer used to find frame time and to have constant frame rate 
	Timer timer;
	timer.start();

	// If true, it draws boudning volumes. Only used for debugging
	bool drawBVs = false;
	
	// Start engine loop
	while (!window.close()){
		// Evaluate the time to draw last frame
		timer.setFrameTime();

		// Get number of frames drawn per second for debugging
		timer.getFPS();

		// Get the time takes to draw the last frame
		frameTime = Timer::m_frameTime;

		// Clear the window buffer to redraw graphics
		window.clearBuffers();

		// Check for keyboard input and move camera
		applyMovement();

		// Check if object is added to scene from the menu window
		if (menuWindow.getOpenedFile()){
			addObject(menuWindow.getSelectedFileToOpen(), glm::vec3(0, 2, 0), glm::vec3(1.0, 1.0, 1.0), Material(), Label::OBJECT);
			menuWindow.setOpenedFile(false);
		}

		// Check if point light was added from the menu window
		if (menuWindow.addedPointLight()){
			renderShader.use();
			PointLight* pointLight = new PointLight(renderShader, lightDirectory.c_str());
			
			if (renderShader.addPointLight(pointLight)){
				renderer.add(pointLight->getModel());
			}

			menuWindow.setAddedPointLight(false);
			renderShader.unuse();
		}

		// Update scene camera
		if (isMWHPressed){
			if (fpsCameraActive){
				fpsCamera.setPitchYaw(mouseX, mouseY);
			}
			else{
				sceneCamera.setPitchYaw(mouseX, mouseY, mouseXIntensity, mouseYIntensity, frameTime);
			}
		}

		if (drawBVs){
			for (Drawable* d : objectManager->getActiveObjects()){
				boundingVolumesShader.use();
				if (fpsCameraActive){
					d->drawBoundingVolume(boundingVolumesShader, renderShader.getProjectionMatrix(), fpsCamera.getViewMatrix());
				}
				else{
					d->drawBoundingVolume(boundingVolumesShader, renderShader.getProjectionMatrix(), sceneCamera.getViewMatrix());
				}
				boundingVolumesShader.unuse();
			}
		}

		// Check for collisions
		checkCollisions();
		collision = false;

		renderShader.use();

		if (fpsCameraActive){
			renderShader.setViewMatrix(fpsCamera.getViewMatrix());
			renderShader.setViewMatrixPosition(fpsCamera.getPosition());
		}
		else{
			renderShader.setViewMatrix(sceneCamera.getViewMatrix());
			renderShader.setViewMatrixPosition(sceneCamera.getPosition());
		}

		// Update Gizmos
		updateGizmos();

		// Update poilt lights
		renderShader.updatePointLights();

		// Draw all objects in the scene
		renderer.bind();
		renderer.process();
		renderer.unbind();
		renderer.draw();
		renderShader.unuse();

		// Draw GUI compnents
		if (!fpsCameraActive){
			guiShader.use();
			for (Drawable* d : guis){
				d->draw();
			}
			guiShader.unuse();
		}

		// Update window
		window.update();
	}

	// Free memory from all 3d objects and gui components
	for (Drawable* d : objectManager->getActiveObjects()){
		delete d;
	}

	for (Drawable* d : guis){
		delete d;
	}

	//delete directionalLight.getModel();

	// Join the menu window thread
	menuThread.join();
	return 0;
}

/*
Sets the path of the main project directory, and the directory of the light object
*/
void setDirectoryPaths(){
	char pathBuffer[MAX_PATH];
	GetModuleFileName(NULL, pathBuffer, MAX_PATH);
	std::string::size_type pathEndIndex = std::string(pathBuffer).find("SRC");
	std::string path = std::string(pathBuffer).substr(0, pathEndIndex).c_str();

	lightDirectory = std::string(path);
	sampleDirectory = std::string(path);

	lightDirectory = lightDirectory.append("SRC\\ge_15\\Models\\Samples\\cube.obj");
	sampleDirectory = sampleDirectory.append("SRC\\ge_15\\Models\\Samples\\");

	guiDirectory = std::string(path);
	guiDirectory.append("SRC\\ge_15\\GUI\\");

}

/*
Initializes OpenGL parameters
*/
void setOpenGLParameters(){
	glfwSetKeyCallback(window.getWindow(), keyCallback);
	glfwSetCursorPosCallback(window.getWindow(), mouseCallback);
	glfwSetMouseButtonCallback(window.getWindow(), mouseButtonCallback);

	if (fpsCameraActive){
		glfwSetInputMode(window.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
}

/*
Initializes shaders and default lights 
*/
void initShaders(){
	// Initialize the scene default point light
	defaultPointLight = new PointLight(renderShader, lightDirectory.c_str(), glm::vec3(0, 4, 0));

	// Set perspective matrix uniform
	renderShader.use();
	renderShader.setPerspectiveMatrixUniform(45.0f, (GLfloat)window.getAspectRatio(), 0.1f, 100.0f);

	// Add directional light 
	renderShader.setDirectionalLightProperties(glm::vec3(0.05f, 0.05f, 0.05f), glm::vec3(0.4f, 0.4f, 0.4f), glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(-0.2f, -1.0f, -0.3f));

	// Add a default point light
	renderShader.addPointLight(defaultPointLight);

	// Add 3d object for light 
	objectManager->add(defaultPointLight->getModel());

	renderShader.unuse();
}

/*
Initializes shaders and default 3d objects
*/
void initDefaultDrawables(){

	// Create first default cube
	Drawable* d1 = new Model(lightDirectory,
		glm::vec3(-3.0, 2.0, 0.0),
		glm::vec3(1.0, 1.0, 1.0));

	// Create second default cube
	Drawable* d2 = new Model(lightDirectory,
		glm::vec3(3.0, 2.0, 0.0),
		glm::vec3(1.0, 1.0, 1.0));

	// Add 3d model for translasion gizmo 
	objectManager->add(gizmo.getModels()[0]);
	objectManager->add(gizmo.getModels()[1]);
	objectManager->add(gizmo.getModels()[2]);

	// Add 3d model for rotation gizmo 
	objectManager->add(gizmo.getRotationModels()[0]);
	objectManager->add(gizmo.getRotationModels()[1]);
	objectManager->add(gizmo.getRotationModels()[2]);

	// Add 3d model for saling gizmo 
	objectManager->add(gizmo.getScaleModels()[0]);
	objectManager->add(gizmo.getScaleModels()[1]);
	objectManager->add(gizmo.getScaleModels()[2]);

	// Create a floor
	std::string floorString(sampleDirectory);
	Drawable* floor = new Model(floorString.append("wooden_floor_2.obj"),
		glm::vec3(0.0, 0, 0.0),
		glm::vec3(1.0, 1.0, 1.0));

	// Add remaining objects
	objectManager->add(d1);
	objectManager->add(d2);
	objectManager->add(floor);
}

/*
Updates the gizmos and selected objects when user is using the gizmo
*/
void updateGizmos(){
	// Update translation gizmo
	if (gizmo.isMoving() == GizmoMovement::GM_X_MOVE){
		gizmo.translate(glm::vec3(mouseXIntensity, 0.0, 0.0), frameTime, objectManager->getSelectedObjects());
	}
	else if (gizmo.isMoving() == GizmoMovement::GM_Y_MOVE){
		gizmo.translate(glm::vec3(0.0, mouseYIntensity, 0.0), frameTime, objectManager->getSelectedObjects());
	}
	else if (gizmo.isMoving() == GizmoMovement::GM_Z_MOVE){
		if (zGizmoHitFromLeft == true){
			gizmo.translate(glm::vec3(0.0, 0.0, mouseXIntensity), frameTime, objectManager->getSelectedObjects());
		}
		else{
			gizmo.translate(glm::vec3(0.0, 0.0, -mouseXIntensity), frameTime, objectManager->getSelectedObjects());
		}
	}
	
	// Update rotation gizmo
	else if (gizmo.isMoving() == GizmoMovement::GM_X_ROT){
		gizmo.rotate(-(mouseYIntensity * rotationSensitivity), glm::vec3(1.0, 0.0, 0.0), objectManager->getSelectedObjects(), frameTime);
	}
	else if (gizmo.isMoving() == GizmoMovement::GM_Y_ROT){
		gizmo.rotate((mouseXIntensity * rotationSensitivity), glm::vec3(0.0, 1.0, 0.0), objectManager->getSelectedObjects(), frameTime);
	}
	else if (gizmo.isMoving() == GizmoMovement::GM_Z_ROT){
		gizmo.rotate((mouseYIntensity * rotationSensitivity), glm::vec3(0.0, 0.0, 1.0), objectManager->getSelectedObjects(), frameTime);
	}
	
	// Update scaling gizmo
	else if (gizmo.isMoving() == GizmoMovement::GM_X_SCALE){
		gizmo.scale(glm::vec3(mouseXIntensity * scaleSensitivity, 0, 0), objectManager->getSelectedObjects(), frameTime);
	}
	else if (gizmo.isMoving() == GizmoMovement::GM_Y_SCALE){
		gizmo.scale(glm::vec3(0, mouseXIntensity * scaleSensitivity, 0), objectManager->getSelectedObjects(), frameTime);
	}
	else if (gizmo.isMoving() == GizmoMovement::GM_Z_SCALE){
		gizmo.scale(glm::vec3(0, 0, mouseXIntensity * scaleSensitivity), objectManager->getSelectedObjects(), frameTime);
	}

	mouseXIntensity = 0.0f;
	mouseYIntensity = 0.0f;
}

/*
Dinamically adds an object to the scene
Input:
	path: The file path of the object to be added
	position: The position of the object
	scale: The scale of the object
	material: The material of the object
	label: The label of the object
Output:
	Returns a pointer to the newly created drawable
*/
Drawable* addObject(const char* path, const glm::vec3 position, const glm::vec3 scale, const Material material, Label label){
	Drawable* d = new Model(path, position, scale, material, label);
	renderer.add(d);
	return d;
}

/*
Performs collision detection
*/
void checkCollisions(){
	for (Drawable* d1 : objectManager->getActiveObjects()){
		BoundingVolume b1 = d1->getBoundingVolume();
		for (Drawable* d2 : objectManager->getActiveObjects()){

			if (d1->getLabel() != Label::X_AXIS && d1->getLabel() != Label::Y_AXIS && d1->getLabel() != Label::Z_AXIS
				&& d2->getLabel() != Label::X_AXIS && d2->getLabel() != Label::Y_AXIS && d2->getLabel() != Label::Z_AXIS
				&& d1->getLabel() != Label::X_SCALE && d1->getLabel() != Label::Y_SCALE && d1->getLabel() != Label::Z_SCALE
				&& d2->getLabel() != Label::X_SCALE && d2->getLabel() != Label::Y_SCALE && d2->getLabel() != Label::Z_SCALE
				&& d1->getLabel() != Label::X_ROT && d1->getLabel() != Label::Y_ROT && d1->getLabel() != Label::Z_ROT
				&& d2->getLabel() != Label::X_ROT && d2->getLabel() != Label::Y_ROT && d2->getLabel() != Label::Z_ROT && d1->getLabel() != Label::LIGHT && d2->getLabel() != Label::LIGHT)
			if (d1->getObjectId() != d2->getObjectId()){
				BoundingVolume b2 = d2->getBoundingVolume();

				float x1 = d1->getPosition().x;
				float x2 = d2->getPosition().x;
				float sizeX1 = b1.getSize().x / 2;
				float sizeX2 = b2.getSize().x / 2;

				float y1 = d1->getPosition().y;
				float y2 = d2->getPosition().y;
				float sizeY1 = b1.getSize().y / 2;
				float sizeY2 = b2.getSize().y / 2;

				float z1 = d1->getPosition().z;
				float z2 = d2->getPosition().z;
				float sizeZ1 = b1.getSize().z / 2;
				float sizeZ2 = b2.getSize().z / 2;

				if (glm::abs(x1 - x2) < (sizeX1 + sizeX2)){
					if (glm::abs(y1 - y2) < (sizeY1 + sizeY2)){
						if (glm::abs(z1 - z2) < (sizeZ1 + sizeZ2)){
							collision = true;
						}
					}
				}

				if (collision){
					d2->isColliding(true);
					for (Model* m : gizmo.getModels()){
						m->isColliding(true);
					}
				}
				else{
					d2->isColliding(false);
					for (Model* m : gizmo.getModels()){
						m->isColliding(false);
					}
				}
			}
		}
	}
}

/*
Creates all the GUI components
*/
void createGUI(){
	// Set the paths of the gui compenents
	std::string eastPanelString(guiDirectory);

	std::string moveButtonStringOff(guiDirectory);
	std::string moveButtonStringOn(guiDirectory);

	std::string rotateButtonStringOff(guiDirectory);
	std::string rotateButtonStringOn(guiDirectory);

	std::string scaleButtonStringOff(guiDirectory);
	std::string scaleButtonStringOn(guiDirectory);

	std::string playButtonString(guiDirectory);

	eastPanelString.append("right_panel.png");

	moveButtonStringOff.append("move_icon_off_2.png");
	moveButtonStringOn.append("move_icon_on_2.png");

	rotateButtonStringOff.append("rotate_icon_off_2.png");
	rotateButtonStringOn.append("rotate_icon_on_2.png");

	scaleButtonStringOff.append("scale_icon_off_2.png");
	scaleButtonStringOn.append("scale_icon_on_2.png");

	playButtonString.append("play_icon_off.png");

	// Model transfomation panel
	Drawable* eastPanel = new Panel(eastPanelString.c_str(), glm::vec3(0.85, -1, 0.0), glm::vec3(0.17, 2, 0.00), guiShader);

	// Transformation buttons
	Drawable* moveButton = new Button(moveButtonStringOff.c_str(), moveButtonStringOn.c_str(), glm::vec3(0.85, 0.8, 0.0), glm::vec3(0.15, 0.20, 0.0), guiShader, Label::MOVE_BUTTON);
	Drawable* rotateButton = new Button(rotateButtonStringOff.c_str(), rotateButtonStringOn.c_str(), glm::vec3(0.85, 0.6, 0.0), glm::vec3(0.15, 0.20, 0.0), guiShader, Label::ROTATE_BUTTON);
	Drawable* scaleButton = new Button(scaleButtonStringOff.c_str(), scaleButtonStringOn.c_str(), glm::vec3(0.85, 0.4, 0.0), glm::vec3(0.15, 0.20, 0.0), guiShader, Label::SCALE_BUTTON);

	// Button to switch to first person view mode
	Drawable* playButton = new Button(playButtonString.c_str(), playButtonString.c_str(), glm::vec3(0.85, 0.2, 0.0), glm::vec3(0.15, 0.20, 0.0), guiShader, Label::PLAY_BUTTON);

	// Add the guis 
	guis.push_back(moveButton);
	guis.push_back(rotateButton);
	guis.push_back(scaleButton);
	guis.push_back(playButton);
	guis.push_back(eastPanel);
}

/*
Shoots a ray and checks what object it intersects with, if any
Input:
	mX: X position of the mouse
	mY: Y position of the mouse
*/
void intersectRay(double mX, double mY){
	// Create ray
	Ray ray;
	if (fpsCameraActive){
		ray = Ray(fpsCamera.getPosition(), renderShader.getProjectionMatrix(), fpsCamera.getViewMatrix(), mouseX, mouseY);
	}
	else{
		ray = Ray(sceneCamera.getPosition(), renderShader.getProjectionMatrix(), sceneCamera.getViewMatrix(), mouseX, mouseY);
	}

	glm::vec3 rayOrigin = ray.getOrigin();
	glm::vec3 rayDir = ray.getDirection();

	float minIntersectionDistance = FLT_MAX;
	Drawable* closestObject = NULL;

	for (Drawable* d : objectManager->getActiveObjects()){
		float distance = d->intersect(ray);

		if (distance < minIntersectionDistance){
			minIntersectionDistance = distance;
			closestObject = d;
		}
	}

	// If ray does not intersect anything, then return
	if (minIntersectionDistance == FLT_MAX){
		objectManager->deselectAll();
		gizmo.clear();
		return;
	}

	// Check which object the ray intersected with
	if (closestObject != NULL){
		// Gizmos
		if (closestObject->getLabel() == Label::X_AXIS){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_X_MOVE);
			}
		}
		else if (closestObject->getLabel() == Label::Y_AXIS){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_Y_MOVE);
			}
		}
		else if (closestObject->getLabel() == Label::Z_AXIS){
			// Find from hich side the z gizmo was hit to prevent objects moving in opposite directions
			glm::vec3 zHit = glm::cross(rayDir, glm::vec3(0, 0, 1));

			if (zHit.y > 0){
				zGizmoHitFromLeft = false;
			}
			else{
				zGizmoHitFromLeft = true;
			}

			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_Z_MOVE);
			}
		}
		else if (closestObject->getLabel() == Label::X_ROT){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_X_ROT);
			}
		}
		else if (closestObject->getLabel() == Label::Y_ROT){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_Y_ROT);
			}
		}
		else if (closestObject->getLabel() == Label::Z_ROT){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_Z_ROT);
			}
		}
		else if (closestObject->getLabel() == Label::X_SCALE){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_X_SCALE);
			}
		}
		else if (closestObject->getLabel() == Label::Y_SCALE){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_Y_SCALE);
			}
		}
		else if (closestObject->getLabel() == Label::Z_SCALE){
			if (isLMBPressed){
				gizmo.setIsMoving(GizmoMovement::GM_Z_SCALE);
			}
		}
		// Other
		else{
			if (closestObject->getIsSelected() == true){
				closestObject->select(false);
				objectManager->deselectObject(closestObject);
				gizmo.centre(objectManager->getSelectedObjects());
			}
			else{
				if (!isLeftShiftPressed){
					objectManager->deselectAll();
				}

				closestObject->select(true);
				objectManager->addSelectedObject(closestObject);
				gizmo.centre(objectManager->getSelectedObjects());
			}
		}
	}
}

/*
Moves the camera according to user input
*/
void applyMovement(){

	if (fpsCameraActive){
		if (input.isKeyPressed(GLFW_KEY_W)){
			fpsCamera.move(Camera::CameraDirection::FORWARD);
		}
		if (input.isKeyPressed(GLFW_KEY_A)){
			fpsCamera.move(Camera::CameraDirection::LEFT);
		}
		if (input.isKeyPressed(GLFW_KEY_S)){
			fpsCamera.move(Camera::CameraDirection::BACK);
		}
		if (input.isKeyPressed(GLFW_KEY_D)){
			fpsCamera.move(Camera::CameraDirection::RIGHT);
		}
	}
	else{
		if (input.isKeyPressed(GLFW_KEY_W)){
			sceneCamera.move(FORWARD);
		}
		if (input.isKeyPressed(GLFW_KEY_A)){
			sceneCamera.move(LEFT);
		}
		if (input.isKeyPressed(GLFW_KEY_S)){
			sceneCamera.move(BACK);
		}
		if (input.isKeyPressed(GLFW_KEY_D)){
			sceneCamera.move(RIGHT);
		}
		if (input.isKeyPressed(GLFW_KEY_LEFT_CONTROL) && isLMBPressed){
			sceneCamera.move(VERTICAL, mouseYIntensity);
		}
	}

}

/*
Callback function for keyboard pressed 
Input:
	window: The window
	key: Key code of pressed key
	scancode: Scancode of pressed key
	action: Type of action (key press, key release, ....)
	mods: 
*/
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
	Window* instance = (Window*)glfwGetWindowUserPointer(window);

	// If escape key pressed, take proper action
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
		if (fpsCameraActive){
			switchCamera(false);
		}
		else{
			glfwSetWindowShouldClose(window, GL_TRUE);
		}
	}

	// Check keys other than escape key
	if (key >= 0 && key < MAX_KEYS){
		if (action == GLFW_PRESS){
			input.press(key);

			if (input.isKeyPressed(GLFW_KEY_DELETE)){
				deleteSelectedObjects();
			}

			if (input.isKeyPressed(GLFW_KEY_LEFT_SHIFT)){
				isLeftShiftPressed = true;
			}
		}
		else if (action == GLFW_RELEASE){
			input.release(key);

			if (key == GLFW_KEY_LEFT_SHIFT){
				isLeftShiftPressed = false;
			}
		}
	}
}


/*
Deletes the object when selected
*/
void deleteSelectedObjects(){
	for (int i = 0; i < objectManager->getSelectedObjects().size(); i++){
		Drawable* d1 = objectManager->getSelectedObjects()[i];
		for (int j = 0; j < objectManager->getActiveObjects().size(); j++){
			Drawable* d2 = objectManager->getActiveObjects()[j];
			if (d1->getObjectId() == d2->getObjectId()){

				if (d2->getLabel() == Label::LIGHT){
					renderShader.use();
					renderShader.removePointLight(d2->getObjectId(), true);
					renderShader.unuse();
				}

				objectManager->deselectObject(d2);
				delete d2;
				objectManager->getActiveObjects().erase(objectManager->getActiveObjects().begin() + j);

				renderer.load();
			}
		}
	}

	gizmo.clear();
}

/*
Updates mouse related variables when mouse is moved
Input: 
	x: x coordinate of mouse
	y: y coordinate of mouse
*/
void updateMouseValues(double x, double y){
	if (isLMBPressed || isMWHPressed){
		mouseXIntensity = (x - lastX);
		mouseYIntensity = (lastY - y);
	}

	mouseX = x;
	mouseY = y;

	lastX = x;
	lastY = y;
}

/*
Updates pitch and yaw of camera when mouse is moved
Input:
	x: x coordinate of mouse
	y: y coordinate of mouse
*/
void updateCamera(double x, double y){
	fpsCamera.setPitchYaw(x, y);
}

/*
Check if gui buttons are clicked
Input:
	x: x coordinate of mouse
	y: y coordinate of mouse
*/
void checkGUIClicks(double x, double y){
	guiShader.use();
	for (Drawable* d : guis){
		d->onMouseHover(x, y);
	}
	guiShader.unuse();
}

/*
Creates a stack of boxes for debugging purposes
*/
void createCrateStack(){
	int rows = 4;
	int cols = 10;
	std::string name = "Models/Samples/textured_cube_";
	std::string ext = ".obj";
	int num = 0;

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			std::string fullName(name);
			fullName.append(std::to_string(num));
			fullName.append(ext);
			Drawable* d = new Model(fullName.c_str(),
				glm::vec3((float)j + (j * 1.5), (float)i + (i * 1), 0.0),
				glm::vec3(1.0, 1.0, 1.0));

			objectManager->add(d);
			num++;
		}
	}
}

/*
Callback function when mouse is moved
Input:
	window: The window
	x: x coordinate of mouse
	y: y coordinate of mouse
*/
void mouseCallback(GLFWwindow* window, double x, double y){
	updateMouseValues(x, y);
	checkGUIClicks(x, y);

	if (fpsCameraActive){
		updateCamera(x, y);
	}
}

/*
Switches between scene camera and first person camera
Input:
	toFpsCamera: True if user switches from scene to first person camera
*/
void switchCamera(bool toFpsCamera){
	if (toFpsCamera){
		glfwSetInputMode(window.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		fpsCameraActive = true;

		// Remove light objects
		std::vector<Drawable*> deleteList;
		for (int i = 0; i < objectManager->getActiveObjects().size(); i++){
			Drawable* d = objectManager->getActiveObjects()[i];

			if (d->getLabel() == Label::LIGHT){
				deleteList.push_back(d);
				renderShader.removePointLight(d->getObjectId(), false);
				objectManager->deselectObject(d);
				delete d;
				objectManager->getActiveObjects().erase(objectManager->getActiveObjects().begin() + i);
				i--;
			}
		}

		// Remove gizmos
		gizmo.clear();

		// Deselect all objects
		objectManager->deselectAll();

		renderer.load();
	}
	else{
		glfwSetInputMode(window.getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		fpsCameraActive = false;
		for (Drawable* d : guis){
			d->select(false);
		}

		// Reload lights
		PointLight** lights = renderShader.getPointLights();

		for (int i = 0; i < renderShader.getPointLightsSize(); i++){
			PointLight* light = lights[i];

			Drawable* d = addObject(lightDirectory.c_str(), light->getPosition(), glm::vec3(0.3f, 0.3f, 0.3f), Material(glm::vec3(100.0f, 100.0f, 100.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f), 2.0f), Label::LIGHT);

			light->setModel(d);
			int id = d->getObjectId();
			light->setIdPtr(&id);
		}

	}
}

/*
Callback function when mouse buttons are pressed
Input:
	window: The window
	button: Which mouse button was pressed
	action: Type of action (button press, button release ....)
	mods:
*/
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods){

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
		for (Drawable* d : guis){
			if (d->isHovered()){
				if (d->getIsSelected()){
					d->select(false);
					gizmo.clear();
					gizmo.setType(GizmoType::G_NONE);
					return;
				}
				else{
					for (Drawable* d2 : guis){
						d2->select(false);
					}

					d->select(true);
					if (d->getLabel() == Label::MOVE_BUTTON){
						gizmo.setType(GizmoType::G_TRANSLATE);
						gizmo.centre(objectManager->getSelectedObjects());
					}
					else if (d->getLabel() == Label::ROTATE_BUTTON){
						gizmo.setType(GizmoType::G_ROTATE);
						gizmo.centre(objectManager->getSelectedObjects());
					}
					else if (d->getLabel() == Label::SCALE_BUTTON){
						gizmo.setType(GizmoType::G_SCALE);
						gizmo.centre(objectManager->getSelectedObjects());
					}
					else if (d->getLabel() == Label::PLAY_BUTTON){
						switchCamera(true);
					}
				}

				return;
			}
		}

		isLMBPressed = true;
		intersectRay(mouseX, mouseY);
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS){
		isMWHPressed = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
		gizmo.setIsMoving(GizmoMovement::NONE);
		isLMBPressed = false;
		mouseXIntensity = 0.0f;
		mouseYIntensity = 0.0f;
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE){
		isMWHPressed = false;
	}
}

// Author: Riad Zitouni

#include "Camera.h"
#include  <math.h>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include "Window.h"
#include "Timer.h"
#include "Logger.h"

namespace ge15{

	Camera::Camera(){
		m_position = glm::vec3(0.0f, 2.0f, 10.0f);
		m_up = glm::vec3(0.0f, 1.0f, 0.0f);
		m_right = glm::vec3(1.0f, 0.0f, 0.0f);
		m_rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
		m_lookAt = glm::vec3(0.0f, 0.0f, 0.0f);
		m_direction = glm::normalize(m_lookAt - m_position);

		m_pitchAngle = 0.0f;
		m_yawAngle = 0.0f;
		
		m_prevX = Window::getWidth() / 2;
		m_prevY = Window::getHeight() / 2;

		m_moveSensitivity = 5.0f;
		m_rotationSensitivity = 0.2;
	}

	Camera::~Camera(){

	}

	void Camera::update(){
		m_direction = glm::normalize(glm::cross(m_up, m_right));

		// Compute pitch axis
		glm::vec3 pitchAxis = glm::cross(m_direction, m_up);

		// Compute pitch quaterion
		glm::quat pitchQuat = glm::angleAxis(m_pitchAngle, pitchAxis);

		// Compute yaw quat
		glm::quat yawQuat = glm::angleAxis(m_yawAngle, m_up);

		// Add the quats
		glm::quat temp = glm::cross(pitchQuat, yawQuat);
		temp = glm::normalize(temp);

		// Update direction
		m_direction = glm::rotate(temp, m_direction);

		m_lookAt = m_position + m_direction;

		// Update right
		m_right = glm::normalize(glm::cross(m_direction, glm::vec3(0.0, 1.0, 0.0)));
		m_up = glm::normalize(glm::cross(m_right, m_direction));

		m_yawAngle = 0.0f;
		m_pitchAngle = 0.0f;
	}


	void Camera::move(CameraDirection direction){
		glm::vec3 forward = glm::cross(m_right, glm::vec3(0.0, 1.0, 0.0));
		if (direction == FORWARD){
			m_position += -forward * m_moveSensitivity * (float)Timer::m_frameTime;
		}
		if (direction == BACK){
			m_position += forward * m_moveSensitivity * (float)Timer::m_frameTime;
		}
		if (direction == LEFT){
			m_position -= m_right * m_moveSensitivity * (float)Timer::m_frameTime;
		}
		if (direction == RIGHT){
			m_position += m_right * m_moveSensitivity * (float)Timer::m_frameTime;
		}

		update();
	}

	void Camera::setPitchYaw(float x, float y){
		
		float offsetX = m_prevX - x;
		float offsetY = m_prevY - y;
	
		m_prevX = x;
		m_prevY = y;

		m_pitchAngle = offsetY  * m_rotationSensitivity * (float)Timer::m_frameTime;
		m_yawAngle = offsetX * m_rotationSensitivity * (float)Timer::m_frameTime;

		update();
	}
	
	glm::mat4 Camera::getViewMatrix() const{
		return glm::lookAt(m_position, m_lookAt, m_up);
	}

}
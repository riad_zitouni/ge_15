// Author: Riad Zitouni

#include "Mesh.h"

namespace ge15{
	Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures)
	{
		m_vertices = vertices;
		m_indices = indices;
		m_textures = textures;
	}
}
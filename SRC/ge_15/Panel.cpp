// Author: Riad Zitouni

#include "Panel.h"

namespace ge15{

	Panel::Panel(const char* texturePath, const glm::vec3& position, const glm::vec3& size, Shader& guiShader, Material material) : Sprite(texturePath, position, size, material, Label::GUI, guiShader){

	}

}
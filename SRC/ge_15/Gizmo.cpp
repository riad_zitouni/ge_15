// Author: Riad Zitouni

#include "Gizmo.h"

#include <iostream>

#include "MathUtils.h"

#include <cfloat>
namespace ge15{

	Gizmo::Gizmo(){

	}

	Gizmo::Gizmo(std::string modelDirectory){
		glm::vec3 ambient(0.1f, 0.1f,0.1f);
		glm::vec3 specular(0.0f, 0.0f, 0.0f);
		Material red(ambient, glm::vec3(1.0, 0.0, 0.0), specular, 0.5);
		Material green(ambient, glm::vec3(0.0, 1.0, 0.0), specular, 0.5);
		Material blue(ambient, glm::vec3(0.0, 0.0, 1.0), specular, 0.5);

		glm::vec3 position = glm::vec3(0,0,0);

		std::string xArrowString = std::string(modelDirectory);
		std::string yArrowString = std::string(modelDirectory);
		std::string zArrowString = std::string(modelDirectory);

		std::string xRotationString = std::string(modelDirectory);
		std::string yRotationString = std::string(modelDirectory);
		std::string zRotationString = std::string(modelDirectory);

		xArrowString.append("x-arrow.obj");
		yArrowString.append("y-arrow.obj");
		zArrowString.append("z-arrow.obj");

		xRotationString.append("x-rotation.obj");
		yRotationString.append("y-rotation.obj");
		zRotationString.append("z-rotation.obj");

		m_xAxis = new Model(xArrowString, position, glm::vec3(1.0, 1.0, 1.0), red, Label::X_AXIS);
		m_yAxis = new Model(yArrowString, position, glm::vec3(1.0, 1.0, 1.0), green, Label::Y_AXIS);
		m_zAxis = new Model(zArrowString, position, glm::vec3(1.0, 1.0, 1.0), blue, Label::Z_AXIS);

		m_xRotation = new Model(xRotationString, position, glm::vec3(1.0, 1.0, 1.0), red, Label::X_ROT);
		m_yRotation = new Model(yRotationString, position, glm::vec3(1.0, 1.0, 1.0), green, Label::Y_ROT);
		m_zRotation = new Model(zRotationString, position, glm::vec3(1.0, 1.0, 1.0), blue, Label::Z_ROT);

		m_xScale = new Model(xArrowString, position, glm::vec3(1.0, 1.0, 1.0), red, Label::X_SCALE);
		m_yScale = new Model(yArrowString, position, glm::vec3(1.0, 1.0, 1.0), green, Label::Y_SCALE);
		m_zScale = new Model(zArrowString, position, glm::vec3(1.0, 1.0, 1.0), blue, Label::Z_SCALE);

		m_translationModels.push_back(m_xAxis);
		m_translationModels.push_back(m_yAxis);
		m_translationModels.push_back(m_zAxis);

		m_rotationModels.push_back(m_xRotation);
		m_rotationModels.push_back(m_yRotation);
		m_rotationModels.push_back(m_zRotation);

		m_scaleModels.push_back(m_xScale);
		m_scaleModels.push_back(m_yScale);
		m_scaleModels.push_back(m_zScale);

		m_moveDirection = GizmoMovement::NONE;
		m_type = GizmoType::G_NONE;

		// Clear so that it's not rendered when created
		clear();
	}

	void Gizmo::translate(glm::vec3 velocity, const float frameTime, std::vector<Drawable*> selectedObjects){
		m_xAxis->setTranslation(velocity, frameTime);
		m_yAxis->setTranslation(velocity, frameTime);
		m_zAxis->setTranslation(velocity, frameTime);

		for (Drawable* drawable : selectedObjects){
			drawable->setTranslation(velocity, frameTime);
		}
	}

	void Gizmo::rotate(const float& angles, const glm::vec3 axis, std::vector<Drawable*> selectedObjects, const float frameTime){
		for (Drawable* drawable : selectedObjects){
			drawable->setRotation(angles, axis, frameTime);
		}
	}

	void Gizmo::scale(const glm::vec3 scale, std::vector<Drawable*> selectedObjects, const float frameTime){
		for (Drawable* drawable : selectedObjects){
			drawable->setScale(scale, frameTime);
		}
	}

	void Gizmo::clear(){
		glm::vec3 position = glm::vec3(FLT_MAX - 1000);
		glm::vec3 scale = glm::vec3(1.0);
		for (Model* model : m_translationModels){
			model->initTransformations(position, scale);
		}

		for (Model* model : m_rotationModels){
			model->initTransformations(position, scale);
		}

		for (Model* model : m_scaleModels){
			model->initTransformations(position, scale);
		}

	}

	void Gizmo::clear(GizmoType type){
		if (type == NONE){
			return;
		}

		glm::vec3 position = glm::vec3(FLT_MAX - 1000);
		glm::vec3 scale(1.0);
		std::vector<Model*> gizmo = getGizmo(type);
		for (Model* model : gizmo){
			model->initTransformations(position, scale);
		}
	}

	std::vector<Model*>& Gizmo::getGizmo(GizmoType type){
		if (type == GizmoType::G_TRANSLATE){
			return m_translationModels;
		}
		else if (type == GizmoType::G_ROTATE){
			return m_rotationModels;
		}
		else if (type == GizmoType::G_SCALE){
			return m_scaleModels;
		}
	}

	void Gizmo::centre(std::vector<Drawable*> selectedObjects){

		std::vector<Model*> gizmo;

		if (m_type == GizmoType::G_TRANSLATE){
			gizmo = m_translationModels;
			clear(GizmoType::G_ROTATE);
			clear(GizmoType::G_SCALE);
		}
		else if (m_type == GizmoType::G_ROTATE){
			gizmo = m_rotationModels;
			clear(GizmoType::G_TRANSLATE);
			clear(GizmoType::G_SCALE);
		}
		else if (m_type == GizmoType::G_SCALE){
			gizmo = m_scaleModels;
			clear(GizmoType::G_TRANSLATE);
			clear(GizmoType::G_ROTATE);
		}
		else{
			return;
		}

		float posX = 0.0;
		float posY = 0.0;
		float posZ = 0.0;

		float scaleX = 0.0;
		float scaleY = 0.0;
		float scaleZ = 0.0;

		int scalar = 1.0;

		if (selectedObjects.size() > 1){
			scalar = 2.0;
		}
		for (Drawable* drawable : selectedObjects){
			posX += drawable->getPosition().x;
			posY += drawable->getPosition().y;
			posZ += drawable->getPosition().z;

			//  times scalar to make it bigger
			scaleX += drawable->getScale().x * scalar;
			scaleY += drawable->getScale().y * scalar;
			scaleZ += drawable->getScale().z * scalar;
		}

		posX /= selectedObjects.size();
		posY /= selectedObjects.size();
		posZ /= selectedObjects.size();

		scaleX /= selectedObjects.size();
		scaleY /= selectedObjects.size();
		scaleZ /= selectedObjects.size();

		glm::vec3 position(posX, posY, posZ);

		glm::vec3 scale(1.0);
		if (scaleX + scaleY + scaleZ > 3){
			scale = glm::vec3(scaleX, scaleY, scaleZ);
		}

		for (Model* model : gizmo){
			model->initTransformations(position, scale);
		}
	}

	Gizmo::~Gizmo(){
	
	}

}
// Author: Riad Zitouni

#include "ObjectManager.h"

namespace ge15{

	ObjectManager* ObjectManager::m_instance = NULL;

	ObjectManager::ObjectManager(){
		m_lastId = 0;
	}

	ObjectManager::~ObjectManager(){
		delete m_instance;
	}

	ObjectManager* ObjectManager::getInstance(){
		if (m_instance == NULL){
			m_instance = new ObjectManager();
		}
		
		return m_instance;
	}

	void ObjectManager::deselectAll(){
		for (Drawable* drawable : m_selectedObjects){
			drawable->select(false);
		}
		m_selectedObjects.clear();
	}

	void ObjectManager::deselectObject(const Drawable* drawable){
		for (int i = 0; i < m_selectedObjects.size(); i++){
			if (drawable->getObjectId() == m_selectedObjects[i]->getObjectId()){
				m_selectedObjects.erase(m_selectedObjects.begin() + i);
				break;
			}
		}
	}

}
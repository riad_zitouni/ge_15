// Author: Riad Zitouni

#include "Window.h"
#include "Logger.h"

#include <iostream>


namespace ge15{

	int Window::m_width;
	int Window::m_height;

	Window::Window(){

		m_title = "Default Window";
		m_width = 1024;
		m_height = 576;

		// Instantiate window
		if (!init()){
			Logger* logger = Logger::getInstance();
			logger->log("ERROR::WINDOW::WINDOW::Failed to initialize GLFW window");
			glfwTerminate();
		}

	}

	Window::Window(const char* title, int width, int height){

		m_title = title;
		m_width = width;
		m_height = height;

		// Instantiate window

		if (!init()){
			Logger* logger = Logger::getInstance();
			logger->log("ERROR::WINDOW::WINDOW::Failed to initialize GLFW window");
			glfwTerminate();
		}
	}

	Window::~Window(){
		glfwTerminate();
	}

	bool Window::init(){
		Logger* logger = Logger::getInstance();

		if (!glfwInit()){
			logger->log("ERROR::WINDOW::INIT::Failed to initialize GLFW");

			return false;
		}

		// Disallow resizing. Must be before creating window
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		m_window = glfwCreateWindow(m_width, m_height, m_title, NULL, NULL);

		if (!m_window){
			logger->log("ERROR::WINDOW::INIT::Failed to create GLFW window");
			return false;
		}


		glfwMakeContextCurrent(m_window);

		// Set pointer to get window instance in callbacks
		glfwSetWindowUserPointer(m_window, this);

		// Set callbacks
		glfwSetFramebufferSizeCallback(m_window, windowResizeCallback);

		// Init glew
		if (glewInit() != GLEW_OK){
			logger->log("ERROR::WINDOW::INIT::Failed to initialize GLEW");
			return false;
		}

		// Set the clear color
		glClearColor(0.3f, 0.4f, 0.4f, 1.0f);

		// Set viewport
		glViewport(0, 0, m_width, m_height);

		// Enable OpenGL options
		glEnable(GL_DEPTH_TEST);
		
		return true;
	}

	void Window::update(){

		// Check fo opengl errors
		GLenum error = glGetError();

		if (error != GL_NO_ERROR){
			Logger* logger = Logger::getInstance();
			logger->log("OpenGL Error", error);
		}

		glfwPollEvents();

		glfwSwapBuffers(m_window);
	}

	int Window::close() const{
		return glfwWindowShouldClose(m_window) == 1;
	}

	void Window::clearBuffers(){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void windowResizeCallback(GLFWwindow* window, int width, int height){
		Window* instance = (Window*)glfwGetWindowUserPointer(window);
		instance->m_width = width;
		instance->m_height = height;
	}

}


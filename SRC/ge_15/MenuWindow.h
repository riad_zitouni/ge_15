/*Class for menu window to add objects to scene*/

// Author: Riad Zitouni

#ifndef MENU_WINDOW
#define MENU_WINDOW

#include <Windows.h>
#include <string>
#include <vector>

namespace ge15{

	enum MenuActions
	{
		FILE_OPEN,
		FILE_EXIT,
		ADD_POINT_LIGHT
	};

	class MenuWindow{

	public:
		MenuWindow();
		MenuWindow(HINSTANCE hInstance, int width = 800, int height = 500, const char* title = "Menu");
		bool init(HINSTANCE hInstance);

		void createMenuContent(const HWND hWindow);
		HMENU addMenuItem(HMENU hmenu, const char* name);
		void addMenuSubItem(HMENU hmenu, MenuActions action, const char* name);

		void initOpenFileDialog();

		void checkErrors();

		static LRESULT windowProc(HWND hWindow, UINT message, WPARAM wParam, LPARAM lParam);

		inline const bool closeWindow() const{ return m_exitButtonClicked; }
		inline const MSG getMessage() const{ return m_message; }
		inline const char* getSelectedFileToOpen() const{ return m_selectedFileToOpen; }
		inline const bool getOpenedFile() const{ return m_openedFile; }
		inline const bool addedPointLight() const{ return m_addedPointLight; }
		inline const HWND getHandle() const{ return m_menuWindowHandle; }
		inline const bool running() const { return m_isRunning; }
		
		inline const void setOpenedFile(bool value) const{ m_openedFile = value; }
		inline const void setAddedPointLight(bool value) const{ m_addedPointLight = value; }

	private:
		int m_width;
		int m_height;
		const char* m_title;

		static bool m_isRunning;
		static bool m_exitButtonClicked;
		static bool m_openedFile;
		static bool m_addedPointLight;

		WNDCLASSEX	m_menuWindowEx;
		HWND m_menuWindowHandle;
		const char* m_menuWindowClassName;
		MSG	m_message;

		static OPENFILENAME m_openFileName;
		char m_fileSize[1024];
		HMENU m_menu;

		static char* m_selectedFileToOpen;

		static int m_textX;
		static int m_textY;
	};
}

#endif
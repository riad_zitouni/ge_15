// Author: Riad Zitouni

#include "SceneCamera.h"
#include  <math.h>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include "Window.h"
#include "Timer.h"

namespace ge15{

	SceneCamera::SceneCamera(){
		m_position = glm::vec3(0.0f, 2.0f, 10.0f);
		m_up = glm::vec3(0.0f, 1.0f, 0.0f);
		m_right = glm::vec3(1.0f, 0.0f, 0.0f);
		m_rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
		m_lookAt = glm::vec3(0.0f, 0.0f, 0.0f);
		m_direction = glm::normalize(m_lookAt - m_position);

		m_pitchAngle = 0.0f;
		m_yawAngle = 0.0f;

		m_prevX = Window::getWidth() / 2;
		m_prevY = Window::getHeight() / 2;

		m_sensitivity = 8.0f;
	}

	SceneCamera::~SceneCamera(){

	}

	void SceneCamera::update(){
		m_direction = glm::normalize(glm::cross(m_up, m_right));

		// Compute neccessary quaternions
		glm::vec3 pitchAxis = glm::cross(m_direction, m_up);
		glm::quat pitchQuat = glm::angleAxis(m_pitchAngle, pitchAxis);
		glm::quat yawQuat = glm::angleAxis(m_yawAngle, m_up);

		// Add the quaternions
		glm::quat temp = glm::cross(pitchQuat, yawQuat);
		temp = glm::normalize(temp);

		// Update direction
		m_direction = glm::rotate(temp, m_direction);

		//add the camera delta
		m_lookAt = m_position + m_direction * 1.0f;

		// Update right
		m_right = glm::normalize(glm::cross(m_direction, glm::vec3(0.0, 1.0, 0.0)));
		m_up = glm::normalize(glm::cross(m_right, m_direction));
	}


	void SceneCamera::move(CameraDirection direction, float intensity){

		if (direction == FORWARD){
			m_position += m_direction * m_sensitivity * (float)Timer::m_frameTime;
		}
		if (direction == BACK){
			m_position += m_direction * -m_sensitivity * (float)Timer::m_frameTime;
		}
		if (direction == LEFT){
			m_position -= m_right * m_sensitivity * (float)Timer::m_frameTime;
		}
		if (direction == RIGHT){
			m_position += m_right * m_sensitivity * (float)Timer::m_frameTime;
		}
		if (direction == VERTICAL){
			m_position += m_up * intensity * m_sensitivity * (float)Timer::m_frameTime;
		}

		update();
	}

	void SceneCamera::setPitchYaw(float x, float y, float xIntensity, float yIntensity, float frameTime){

		float offsetX = (m_prevX - x);
		float offsetY = (m_prevY - y);

		m_prevX = x;
		m_prevY = y;

		float damping = 0.01;

		m_pitchAngle = offsetY * damping * glm::abs(yIntensity) * frameTime;
		m_yawAngle = offsetX * damping * glm::abs(xIntensity) * frameTime;

		update();
	}

	glm::mat4 SceneCamera::getViewMatrix() const{
		return glm::lookAt(m_position, m_lookAt, m_up);
	}

}
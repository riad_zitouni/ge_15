// Author: Riad Zitouni

#include "DirectionalLight.h"

namespace ge15{

	DirectionalLight::DirectionalLight(){

	}

	DirectionalLight::DirectionalLight(const Shader& shader, const glm::vec3& position, const glm::vec3& direction, Material material, std::string path) : Light(shader, position, true, material, "Models/Samples/cube.obj"){
		m_direction = direction;
	}

}
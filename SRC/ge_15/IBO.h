/*Class for the index buffer*/

// Author: Riad Zitouni

#ifndef IBO_H
#define IBO_H

#include <GL\glew.h>

namespace ge15{
	class IBO{

	public:
		IBO();
		IBO(GLuint* data, GLsizei size);
		~IBO();
	
		void bind() const;
		void unbind() const;

		void setData(GLuint* data, GLsizei size);

		// Just for unit testing
		inline const GLuint getId() const { return m_id; }
		inline const GLuint getCount() const { return m_count; }

	private:
		GLuint m_id;
		GLuint m_count;
	};
}

#endif
#version 330 core
layout (location = 0) in vec3 l_position;
layout (location = 1) in vec3 l_normal;
layout (location = 2) in vec2 l_uv;
layout (location = 3) in float l_tid;
layout (location = 4) in vec3 l_ambient;
layout (location = 5) in vec3 l_diffuse;
layout (location = 6) in vec3 l_specular;
layout (location = 7) in float l_shininess;
layout (location = 8) in float l_isSelected;

out Material{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
}mat_out;

out vec3 in_normal;
out vec3 in_position;
out vec2 texCoord;
out float tid;

out float isSelected;

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;

void main()
{
    gl_Position = u_projectionMatrix * u_viewMatrix * vec4(l_position, 1.0f);
	
	in_position = l_position;
	in_normal= l_normal;
	texCoord = vec2(l_uv.x, l_uv.y);
	tid = l_tid;
    
	// Set material properties	
	mat_out.ambient = l_ambient;
	mat_out.diffuse = l_diffuse;
	mat_out.specular = l_specular;
	mat_out.shininess = l_shininess;

	isSelected = l_isSelected;
}
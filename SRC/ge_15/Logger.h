/* Class for logging errors or printing text to the menu window when debugging.

- Format of error log is:
ERROR::<Class Name>::<Method Name>::<Error Message>
*/

// Author: Riad Zitouni

#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <mutex>

#include <glm\glm.hpp>
#include <GL\glew.h>

namespace ge15{
	class Logger{

#define MAX_LOG_SIZE 20

	public:

		void log(char* message);
		void log(const char* message);
		void log(const char* part1,const char* part2);
		void log(const char* part1, GLenum part2);
		void log(const char* part1, float part2);
		void log(const glm::vec3 vector);

		void makeSpace();
		void clear();

		inline const bool update() { return m_update; }
		inline void setUpdate(bool value) { m_update = value; }
		inline const std::vector<std::string>& getLog() {return m_log; }
		static Logger* getInstance();

		std::mutex m_mutex;
	private:
		Logger();
		~Logger();
		static Logger* m_instance;
		std::vector<std::string> m_log;
		bool m_update;


	};
}

#endif
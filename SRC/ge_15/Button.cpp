// Author: Riad Zitouni

#include "Button.h"
#include "Window.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SOIL.h>

#include <iostream>

namespace ge15{

	Button::Button(const char* offIconPath, const char* onIconPath, glm::vec3 position, glm::vec3 size, Shader& guiShader, Label label, Material material)
		: Sprite(offIconPath, position, size, material, label, guiShader)
	{
		init();
		m_onIconID = loadTexture(onIconPath);
	}

	void Button::draw(){
		if (getIsSelected()){
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_onIconID);

			m_shader.uniform1i("u_texture", 0);

			glBindVertexArray(m_VAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
		else{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_textureID);

			m_shader.uniform1i("u_texture", 0);

			glBindVertexArray(m_VAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
	}
}
/*Class for directional light*/

// Author: Riad Zitouni

#ifndef DIRECTIONAL_LIGHT_H
#define DIRECTIONAL_LIGHT_H

#include "Light.h"

namespace ge15{
	class DirectionalLight : public Light{

	public:
		DirectionalLight();

		DirectionalLight(const Shader& shader, const glm::vec3& position, const glm::vec3& direction, Material material =
			Material(glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(0.8f, 0.4f, 0.4f),
			glm::vec3(0.5f, 0.5f, 0.5f), 1.0), std::string path = "");

		const inline glm::vec3& getDirection()const{ return m_direction; }

	private:
		glm::vec3 m_direction;
	};
}

#endif
/*Class for 3d meshes. Stores all vertices, indices, and textures*/

// Author: Riad Zitouni

#ifndef MESH_H
#define MESH_H

#include <vector>

#include <glm\glm.hpp>

#include <GL\glew.h>

#include <assimp/postprocess.h>

namespace ge15{

	struct Texture {
		GLuint id;
		std::string type;
		aiString path;
	};

	struct Vertex {
		glm::vec3 position;
		glm::vec3 normal;
		
		glm::vec2 texCoords;
		float texId;

		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
		float shininess;

		float isSelected = 0.0f;
	};

	class Mesh{

	public:

		Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures);

		std::vector<Vertex> m_vertices;
		std::vector<GLuint> m_indices;
		std::vector<Texture> m_textures;

	};
}

#endif
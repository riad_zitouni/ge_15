/*Class for gui buttons*/

// Author: Riad Zitouni

#ifndef BUTTON_H
#define BUTTON_H

#include "Sprite.h"
#include "Shader.h"

namespace ge15{

	class Button : public Sprite{

	public:

		Button(const char* offIconPath, const char* onIconPath, glm::vec3 position, glm::vec3 size, Shader& guiShader, Label label = Label::GUI, Material material = Material());
		
		void draw() override;
		
	private:

		GLuint m_onIconID;

		bool m_isOn;

	};
}

#endif
#version 330 core
in vec3 in_position;
in vec3 in_normal;
in vec2 texCoord;
in float tid;
in float isSelected;

out vec4 color;

in Material{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
}in_mat;

struct DirectionalLight{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

#define MAX_POINT_LIGHTS 10

vec3 processDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDirection);
vec3 processPointLight(PointLight light, vec3 normal, vec3 position, vec3 viewDirection);

uniform DirectionalLight u_directionalLight;

uniform PointLight u_pointLights[MAX_POINT_LIGHTS];

uniform vec3 u_viewPosition;

uniform sampler2D u_textures[32];

uniform float u_activePointLights;

void main()
{
	vec3 normal = normalize(in_normal);
	vec3 viewDirection = normalize(u_viewPosition - in_position);
	vec3 result = processDirectionalLight(u_directionalLight, normal, viewDirection);
	
	for(int i = 0; i < int(u_activePointLights); i++){
		result += processPointLight(u_pointLights[i], normal, in_position, viewDirection);
	}

	// Check if object is selected
	vec4 selectionColor = vec4(1.0, 1.0, 1.0, 1.0);
	if(isSelected > 0.0){
		selectionColor = vec4(6.0, 0.5, 0.0, 0.0);
		color = selectionColor;
		return;
	}

	vec4 finalColor;
	if(tid > 1.0){
		int tidi = int(tid - 0.5); 
		finalColor = vec4(result, 1.0f) * texture(u_textures[tidi], texCoord);
	}else{
		finalColor = vec4(result, 1.0f);
 	}

	// Apply Gamma correction
	finalColor = pow(finalColor, vec4(1.0/1.5));
	
	color = finalColor;
}

vec3 processDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDirection){
	vec3 lightDirection = normalize(-light.direction);
		
	// Diffuse
	float difference = max(dot(normal, lightDirection), 0.0);

	// Specular
	vec3 halfWayDirection = normalize(lightDirection + viewDirection);
	float spec = pow(max(dot(normal, halfWayDirection), 0.0), in_mat.shininess);

	vec3 ambient = light.ambient * in_mat.ambient;
	vec3 diffuse = light.diffuse * difference * in_mat.diffuse;
	vec3 specular = light.specular * light.diffuse * spec * in_mat.specular;

	return (ambient + diffuse + specular);
}

vec3 processPointLight(PointLight light, vec3 normal, vec3 position, vec3 viewDirection){
	
	vec3 lightDirection = normalize(light.position - position);
		
	float dot_product = dot(lightDirection, normal);
	
	// Diffuse
	float difference = max(dot(normal, lightDirection), 0.0);

	// Specular
	vec3 halfWayDirection = normalize(lightDirection + viewDirection);
	float spec = pow(max(dot(normal, halfWayDirection), 0.0), in_mat.shininess);

	// Attenuation
	float distance = length(light.position - position);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * distance );

	vec3 ambient = light.ambient * in_mat.ambient * attenuation;
	vec3 diffuse = light.diffuse * difference * in_mat.diffuse * attenuation;
	vec3 specular = light.specular * spec * light.diffuse * in_mat.specular * attenuation;


	return (ambient + diffuse + specular);
}
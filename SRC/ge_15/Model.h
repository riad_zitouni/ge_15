/*Class for loading the 3d model using the Assimp library*/

// Author: Riad Zitouni

/*Note: The methods loadModel, processNode, processMesh, loadMaterialTextures and TextureFromFile were obtained
from http://www.learnopengl.com/#!Model-Loading/Assimp. I made small modifications to some of the methods*/


#ifndef MODEL_H
#define MODEL_H

#include "Drawable.h"
#include <assimp/scene.h>

namespace ge15{
	class Model : public Drawable{

	public:
		Model();
		Model(std::string path, glm::vec3 position, glm::vec3 size, Material material = Material(), Label label = Label::OBJECT);

		float intersect(const Ray& ray) override;

	private:
		void loadModel(std::string path);
		void processNode(aiNode* node, const aiScene* scene);
		Mesh processMesh(aiMesh* mesh, const aiScene* scene);
		GLint TextureFromFile(const char* path, std::string directory);
		Texture loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);

		std::string m_directory;
		static std::vector<Texture> textures_loaded;

	};
}

#endif
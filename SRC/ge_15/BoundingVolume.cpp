// Author: Riad Zitouni

#include "BoundingVolume.h"

#include <iostream>
#include "MathUtils.h"

#include <cfloat>

namespace ge15{

	BoundingVolume::BoundingVolume(){
	}

	BoundingVolume::BoundingVolume(std::vector<Vertex> vertices){
		createBoundingVolume(vertices);
		init();
	}

	void BoundingVolume::createBoundingVolume(std::vector<Vertex> vertices){
		// Find smallest and largest vertex
		glm::vec3 minVertex = vertices[0].position;
		glm::vec3 maxVertex = vertices[0].position;

		for (Vertex vertex : vertices){
			// Set the smallest
			glm::vec3 vPos = vertex.position;
			
			if (vPos.x < minVertex.x){
				minVertex.x = vPos.x;
			}
			if (vPos.y < minVertex.y){
				minVertex.y = vPos.y;
			}
			if (vPos.z < minVertex.z){
				minVertex.z = vPos.z;
			}

			if (vPos.x > maxVertex.x){
				maxVertex.x = vPos.x;
			}
			if (vPos.y > maxVertex.y){
				maxVertex.y = vPos.y;
			}
			if (vPos.z > maxVertex.z){
				maxVertex.z = vPos.z;
			}
		}

		m_minVertex = minVertex;
		m_maxVertex = maxVertex;
		
		// Front
		m_bvVertices.push_back(glm::vec3(minVertex.x, minVertex.y, minVertex.z));
		m_bvVertices.push_back(glm::vec3(minVertex.x, maxVertex.y, minVertex.z));
		m_bvVertices.push_back(glm::vec3(maxVertex.x, maxVertex.y, minVertex.z));
		m_bvVertices.push_back(glm::vec3(maxVertex.x, minVertex.y, minVertex.z));

		// Back Vertices
		m_bvVertices.push_back(glm::vec3(minVertex.x, minVertex.y, maxVertex.z));
		m_bvVertices.push_back(glm::vec3(maxVertex.x, minVertex.y, maxVertex.z));
		m_bvVertices.push_back(glm::vec3(maxVertex.x, maxVertex.y, maxVertex.z));
		m_bvVertices.push_back(glm::vec3(minVertex.x, maxVertex.y, maxVertex.z));

		// Front Face
		m_bvIndex[0] = 0; m_bvIndex[1] = 1; m_bvIndex[2] = 2;
		m_bvIndex[3] = 0; m_bvIndex[4] = 2; m_bvIndex[5] = 3;

		// Back Face
		m_bvIndex[6] = 4; m_bvIndex[7] = 5; m_bvIndex[8] = 6;
		m_bvIndex[9] = 4; m_bvIndex[10] = 6; m_bvIndex[11] = 7;

		// Top Face
		m_bvIndex[12] = 1; m_bvIndex[13] = 7; m_bvIndex[14] = 6;
		m_bvIndex[15] = 1; m_bvIndex[16] = 6; m_bvIndex[17] = 2;

		// Bottom Face
		m_bvIndex[18] = 0; m_bvIndex[19] = 4; m_bvIndex[20] = 5;
		m_bvIndex[21] = 0; m_bvIndex[22] = 5; m_bvIndex[23] = 3;

		// Left Face
		m_bvIndex[24] = 4; m_bvIndex[25] = 7; m_bvIndex[26] = 1;
		m_bvIndex[27] = 4; m_bvIndex[28] = 1; m_bvIndex[29] = 0;

		// Right Face
		m_bvIndex[30] = 3; m_bvIndex[31] = 2; m_bvIndex[32] = 6;
		m_bvIndex[33] = 3; m_bvIndex[34] = 6; m_bvIndex[35] = 5;

	}

	void BoundingVolume::setSize(const glm::mat4& modelMatrix){
		std::vector<glm::vec3> worldVertices;

		for (glm::vec3 vertex : m_bvVertices){
			glm::vec4 newVertex = modelMatrix * glm::vec4(vertex, 1.0);
			worldVertices.push_back(glm::vec3(newVertex));
		}

		// Find smallest and largest vertex
		glm::vec3 minVertex = worldVertices[0];
		glm::vec3 maxVertex = worldVertices[0];

		for (glm::vec3 vertex : worldVertices){
			if (vertex.x < minVertex.x){
				minVertex.x = vertex.x;
			}
			if (vertex.y < minVertex.y){
				minVertex.y = vertex.y;
			}
			if (vertex.z < minVertex.z){
				minVertex.z = vertex.z;
			}

			if (vertex.x > maxVertex.x){
				maxVertex.x = vertex.x;
			}
			if (vertex.y > maxVertex.y){
				maxVertex.y = vertex.y;
			}
			if (vertex.z > maxVertex.z){
				maxVertex.z = vertex.z;
			}
		}

		float sizeX = glm::abs(maxVertex.x - minVertex.x);
		float sizeY = glm::abs(maxVertex.y - minVertex.y);
		float sizeZ = glm::abs(maxVertex.z - minVertex.z);

		m_size = glm::vec3(sizeX, sizeY, sizeZ);
	}

	void BoundingVolume::init(){
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);
		glBindVertexArray(VAO);

		GLfloat vertices[] = {
			m_bvVertices[0].x, m_bvVertices[0].y, m_bvVertices[0].z,
			m_bvVertices[1].x, m_bvVertices[1].y, m_bvVertices[1].z,
			m_bvVertices[2].x, m_bvVertices[2].y, m_bvVertices[2].z,
			m_bvVertices[3].x, m_bvVertices[3].y, m_bvVertices[3].z,
			m_bvVertices[4].x, m_bvVertices[4].y, m_bvVertices[4].z,
			m_bvVertices[5].x, m_bvVertices[5].y, m_bvVertices[5].z,
			m_bvVertices[6].x, m_bvVertices[6].y, m_bvVertices[6].z,
			m_bvVertices[7].x, m_bvVertices[7].y, m_bvVertices[7].z
		};

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(m_bvIndex), m_bvIndex, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, 0); 

		glBindVertexArray(0); 
	}

	float BoundingVolume::intersect(const Ray& ray, const glm::vec3& position, const glm::vec3& scale){

		const glm::vec3 rayOrigin = ray.getOrigin();
		const glm::vec3 rayDir = ray.getDirection();

		glm::mat4 modelMatrix;
		modelMatrix = glm::translate(modelMatrix, position);
		modelMatrix = glm::scale(modelMatrix, scale);

		for (int i = 0; i < 36; i += 3){

			glm::vec3 tp1 = glm::vec3(modelMatrix * glm::vec4((m_bvVertices[m_bvIndex[i]]), 1.0));
			glm::vec3 tp2 = glm::vec3(modelMatrix * glm::vec4((m_bvVertices[m_bvIndex[i + 1]]), 1.0));
			glm::vec3 tp3 = glm::vec3(modelMatrix * glm::vec4((m_bvVertices[m_bvIndex[i + 2]]), 1.0));

			// 1- Genereate face normal
			glm::vec3 normal = glm::cross((tp3 - tp1), (tp2 - tp1));
			normal = glm::normalize(normal);

			// 2- Find D in (Ax + By + Cz + D = 0) where A and B and C are the normal components
			float D = -(normal.x * tp1.x) - (normal.y * tp1.y) - (normal.z * tp1.z);

			// 3- Find t
			float ep1 = (rayOrigin.x * normal.x) + (rayOrigin.y * normal.y) + (rayOrigin.z * normal.z);
			float ep2 = (rayDir.x * normal.x) + (rayDir.y * normal.y) + (rayDir.z * normal.z);

			float t = -(ep1 + D) / (ep2);

			// 4- Find intersection point on plane
			glm::vec3 intersectionPoint = rayOrigin + (rayDir * t);

			if (t > 0.0f){
				if (isIntersectionInTriangle(intersectionPoint, tp1, tp2, tp3)){
					return t;
				}
			}
		}

		return FLT_MAX;
	}

	bool BoundingVolume::isIntersectionInTriangle(const glm::vec3 intersectionPoint, glm::vec3& tp1, const glm::vec3& tp2, const glm::vec3& tp3){
		glm::vec3 cp1 = glm::cross((intersectionPoint - tp2), (tp3 - tp2));
		glm::vec3 cp2 = glm::cross((tp1 - tp2), (tp3 - tp2));

		if (glm::dot(cp1, cp2) >= 0){
			cp1 = glm::cross((tp3 - tp1), (intersectionPoint - tp1));
			cp2 = glm::cross((tp3 - tp1), (tp2 - tp1));

			if (glm::dot(cp1, cp2) >= 0){
				cp1 = glm::cross((intersectionPoint - tp1), (tp2 - tp1));
				cp2 = glm::cross((tp3 - tp1), (tp2 - tp1));

				if (glm::dot(cp1, cp2) >= 0){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	void BoundingVolume::draw(){
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

}
/*This class is for rays*/

// Author: Riad Zitouni

#ifndef RAY_H
#define RAY_H

#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace ge15{
	class Ray{

	public:

		Ray();
		Ray(glm::vec3 origin, const glm::mat4& projectionMatrix = glm::mat4(), const glm::mat4& viewMatrix = glm::mat4(), double mouseX = 0, double mouseY = 0);

		inline const glm::vec3 getOrigin() const{ return m_origin; }
		inline const glm::vec3 getDirection() const{ return m_direction; }

	private:
		// Sets the direction in world coordinate from the mouse click (viewport coords)
		void setDirectionFromMouse(double mouseX, double mouseY, const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix); 
		glm::vec3 m_origin;
		glm::vec3 m_direction;
	};
}

#endif
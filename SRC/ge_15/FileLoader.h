/*Utility class for loading and reading files*/

// Author: Riad Zitouni

#ifndef FILE_LOADER_H
#define FILE_LOADER_H

#include <string>
#include <iostream>
#include <string>

#include "Logger.h"

namespace ge15{

	// Read non-image files
	static bool readFile(const char* path, std::string& output){
		Logger* logger = Logger::getInstance();

		FILE* file = fopen(path, "rt");
		int result = 0;

		if (file == NULL){
			logger->log("ERROR::FILE_LOADER::READ_FILE::Failed to open file: ", path);
			return false;
		}

		// Get file size
		result = fseek(file, 0, SEEK_END);

		if (result != 0){
			logger->log("ERROR::FILE_LOADER::READ_FILE::Failed to seek file size of: ", path);
			return false;
		}

		unsigned long size = ftell(file);
		size++; // To allocate space for termination character

		char* data = new char[size];
		
		// Remove unwanted chars
		memset(data, 0, size);

		// Go to begining of file
		result = fseek(file, 0, SEEK_SET);

		if (result != 0){
			logger->log("ERROR::FILE_LOADER::READ_FILE::Failed to seek file size of: ", path);
			return false;
		}

		// Read file
		result = fread(data, 1, size - 1, file);

		if (result == 0){
			logger->log("WARNING::FILE_LOADER::READ_FILE::Empty File: ", path);
		}

		// Close file
		fclose(file);

		output = std::string(data);
		delete[] data;

		return true;
	}

	static bool fileExists(std::string path){
		FILE* file = fopen(path.c_str(), "r");
		if (file){
			fclose(file);
			return true;
		}
		else{
			return false;
		}
	}

}

#endif
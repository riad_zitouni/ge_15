**Summary:**
This is my ongoing project to create a 3d game engine using C++ and OpenGL.

**Controls:**
Use WASD controls to move camera and move the mouse while pressing the mouse wheel to rotate the camera.

Press left-Ctrl + LMB and move the mouse to move the camera vertically.

To explore the scene in first-person view, click the play button located on the 
right panel (4th button from the top). Then use WASD and the mouse to walk around. Then press 
the escape key to exit first-person view.

To move, rotate, or scale an object, select an object by clicking on it 
(the object should turn orange), then click on one of the first three buttons on the right panel. 
The buttons from top to bottom are for translation, rotation and scaling. 
After selecting one of the buttons, use the gizmo that appears to apply the transformation.

Press left-shift + LMB to select multiple objects.

To add an object, click on File->Open in the menu window and select the object (.obj file). 
Note that only objects with the .obj format can be loaded for now.

To add a light, click on Add->"Point Light" in the menu window.

To delete an object, select it first and press delete.

**Running the engine:**
To run the game engine, double click on ge_15.exe in SRC/bin/Debug or SRC/bin/Release

**Notes:**
All 3d models and textures in the source files were created by me
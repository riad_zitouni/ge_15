/*Class for scene camera*/

// Author: Riad Zitouni

#ifndef SCENE_CAMERA_H
#define SCENE_CAMERA_H

#include <glm\glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace ge15{

#define MAX_PITCH_ANGLE 89

	enum CameraDirection{
		FORWARD,
		BACK,
		LEFT,
		RIGHT,
		VERTICAL
	};

	class SceneCamera{

	public:

		SceneCamera();
		~SceneCamera();

		void update();

		void move(CameraDirection direction, float intensity = 0.0f);
		void setPitchYaw(float x, float y, float xIntensity, float yIntensity, float frameTime);
		void setYaw(float angle);

		glm::mat4 getViewMatrix() const;

		inline const glm::vec3 getPosition() const{ return m_position; }
		inline const glm::vec3 getDirection() const{ return m_direction; }
		inline const float getPitchAngle() const{ return m_pitchAngle; }
		inline const float getYawAngle() const{ return m_yawAngle; }

	private:

		float m_prevX;
		float m_prevY;

		float m_pitchAngle;
		float m_yawAngle;

		float m_sensitivity;

		glm::vec3 m_position;
		glm::vec3 m_lookAt;
		glm::vec3 m_direction;
		glm::vec3 m_up;
		glm::vec3 m_right;

		glm::quat m_rotation;

	};

}

#endif
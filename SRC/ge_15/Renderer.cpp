// Author: Riad Zitouni

#include "Renderer.h"
#include "Model.h"
#include "Timer.h"
#include "MathUtils.h"
#include "Logger.h"
#include <iostream>

namespace ge15{

	Renderer::Renderer(){
		init();

		m_objectManager = ObjectManager::getInstance();
	}

	Renderer::~Renderer(){
		delete m_IBO;
		glDeleteBuffers(1, &m_VBO);
	}
	
	void Renderer::init(){
		glGenVertexArrays(1, &m_VAO);
		glGenBuffers(1, &m_VBO);

		// applies now the attrib stuff to this VAO and  not to the general one
		glBindVertexArray(m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, NULL, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(SHADER_POSITION_LOCATION); // position
		glEnableVertexAttribArray(SHADER_NORMAL_LOCATION);
		glEnableVertexAttribArray(SHADER_UV_LOCATION);
		glEnableVertexAttribArray(SHADER_TEXID_LOCATION);
		glEnableVertexAttribArray(SHADER_AMBIENT_LOCATION);
		glEnableVertexAttribArray(SHADER_DIFFUSE_LOCATION);
		glEnableVertexAttribArray(SHADER_SPECULAR_LOCATION);
		glEnableVertexAttribArray(SHADER_SHININESS_LOCATION);
		glEnableVertexAttribArray(SHADER_IS_SELECTED_LOCATION);
		
		// Position
		glVertexAttribPointer(SHADER_POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)0);

		// Normal
		glVertexAttribPointer(SHADER_NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::normal));

		// Texture coords
		glVertexAttribPointer(SHADER_UV_LOCATION, 2, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::texCoords));

		// Texture id
		glVertexAttribPointer(SHADER_TEXID_LOCATION, 1, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::texId));

		// AMBIENT
		glVertexAttribPointer(SHADER_AMBIENT_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::ambient));

		// Diffuse
		glVertexAttribPointer(SHADER_DIFFUSE_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::diffuse));

		// SPECULAR
		glVertexAttribPointer(SHADER_SPECULAR_LOCATION, 3, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::specular));

		// Shininess
		glVertexAttribPointer(SHADER_SHININESS_LOCATION, 1, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::shininess));

		// Is object selected flag
		glVertexAttribPointer(SHADER_IS_SELECTED_LOCATION, 1, GL_FLOAT, GL_FALSE, VERTEX_SIZE, (const GLvoid*)offsetof(Vertex, Vertex::isSelected));

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Renderer::load(){
		GLuint indices[INDEX_BUFFER_SIZE];
		m_indexCount = 0;
		m_indexOffset = 0;
		for (int i = 0; i < INDEX_BUFFER_SIZE; i++){
			m_totalIndices[i] = 0;
		}

		for (Drawable* d : m_objectManager->getActiveObjects()){
			if (m_indexCount > 0){
				d->setIndex(m_indexCount + 1);
			}
			for (Mesh mesh : d->meshes){
				for (int i = 0; i < mesh.m_indices.size(); i++){
					m_totalIndices[m_indexCount + i] = (mesh.m_indices[i] + m_indexOffset);
				}

				m_indexOffset += mesh.m_vertices.size();
				m_indexCount += mesh.m_indices.size();
			}
		}

		m_IBO = new IBO(m_totalIndices, INDEX_BUFFER_SIZE);
		glBindVertexArray(0);
	}

	void Renderer::add(Drawable* drawable){
		if (m_indexCount > 0){
			drawable->setIndex(m_indexCount + 1);
		}

		m_IBO->bind();
		for (Mesh mesh : drawable->meshes){
			for (int i = 0; i < mesh.m_indices.size(); i++){
				m_totalIndices[m_indexCount + i] = (mesh.m_indices[i] + m_indexOffset);
			}

			m_indexOffset += mesh.m_vertices.size();
			m_indexCount += mesh.m_indices.size();
		}
		m_IBO->setData(m_totalIndices, INDEX_BUFFER_SIZE);
		m_IBO->unbind();

		m_objectManager->add(drawable);
	}

	void Renderer::process(){
		for (Drawable* drawable : m_objectManager->getActiveObjects()){
			glm::mat4 modelMatrix = drawable->getModelMatrix();
			const Material& material = drawable->getMaterial();

			for (Mesh mesh : drawable->meshes){
				const GLuint texId = mesh.m_textures[0].id;

				// Check if texture is already in list
				bool texExists = false;
				for (int i = 1; i < MAX_TEXTURES; i++){
					if (m_activeTextures[i] == texId && texId > 0){
						texExists = true;
						break;
					}
				}

				if (!texExists && texId > 0 && texId ){
					m_activeTextures[texId] = texId;
				}

				for (unsigned int i = 0; i < mesh.m_vertices.size(); i++){

					float x = mesh.m_vertices[i].position.x;
					float y = mesh.m_vertices[i].position.y;
					float z = mesh.m_vertices[i].position.z;
					const glm::vec2& texCoords = mesh.m_vertices[i].texCoords;
					const glm::vec3& normal = mesh.m_vertices[i].normal;

					glm::vec4 vertexPos = glm::vec4(mesh.m_vertices[i].position, 1.0);
					
					glm::vec4 result = modelMatrix * vertexPos;

					m_vertexBuffer->position = (glm::vec3(result));
					m_vertexBuffer->normal = glm::vec3(normal);
					m_vertexBuffer->texCoords = texCoords;
					m_vertexBuffer->texId = (float) (texId + 1); 

					// Set material properties
					m_vertexBuffer->ambient = material.ambient;
					m_vertexBuffer->diffuse = material.diffuse;
					m_vertexBuffer->specular = material.specular;
					m_vertexBuffer->shininess = material.shininess;

					// Set selected
					m_vertexBuffer->isSelected = drawable->getIsSelected();

					m_vertexBuffer++;
				}
			}

		}
	}

	void Renderer::draw(){
		for (int i = 0; i < MAX_TEXTURES; i++){
			glActiveTexture(GL_TEXTURE0 + m_activeTextures[i]);
			glBindTexture(GL_TEXTURE_2D, m_activeTextures[i]);
		}

		glBindVertexArray(m_VAO);
		m_IBO->bind();

		glDrawElements(GL_TRIANGLES, m_indexCount, GL_UNSIGNED_INT, NULL);

		m_IBO->unbind();
		glBindVertexArray(0);
	}

	void Renderer::bind(){
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		m_vertexBuffer = (Vertex*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	}

	void Renderer::unbind(){
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}


}

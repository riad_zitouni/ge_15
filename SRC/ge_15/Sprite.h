/*Super class of all gui components*/

// Author: Riad Zitouni

#ifndef SPRITE_H
#define SPRITE_H

#include "Drawable.h"

namespace ge15{
	class Sprite : public Drawable{
	public:
		Sprite(const char* texturePath, glm::vec3 position, glm::vec3 size, Material material, Label label, Shader& guiShader);
		~Sprite() override;

		void init();
		void draw() override;

		GLuint loadTexture(const char* path);
	private:
		
	protected:
		Shader m_shader;
		GLuint m_VBO, m_VAO;
		GLuint m_textureID;
	};
}

#endif
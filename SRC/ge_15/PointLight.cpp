// Author: Riad Zitouni

#include "PointLight.h"

namespace ge15{
	
	PointLight::PointLight(){
		glm::vec3 zeroVector = glm::vec3(0, 0, 0);
		m_lightMaterial = Material(zeroVector, zeroVector, zeroVector, 0.0f);
		m_constant = m_linear = m_quadratic = 0.0f;
	}

	PointLight::PointLight(const Shader& shader, const char* path, const glm::vec3& position, float constant, float linear, float quadratic, Material material) : Light(shader, position, true, material, path){
		m_constant = constant;
		m_linear = linear;
		m_quadratic = quadratic;
	}
}
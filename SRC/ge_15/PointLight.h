/*Class for point lights*/

// Author: Riad Zitouni

#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H

#include "Light.h"

namespace ge15{
	class PointLight : public Light{

	public:
		PointLight();

		PointLight(const Shader& shader, const char* path, const glm::vec3& position = glm::vec3(0, 2, 2), float constant = 1.0f, float linear = 0.09f, float quadratic = 0.032f, Material material =
			Material(glm::vec3(0.05f, 0.05f, 0.05f),
			glm::vec3(0.8f, 0.8f, 0.8f),
			glm::vec3(1.0f, 1.0f, 1.0f), 1.0));

		inline float const getConstant() const{ return m_constant; };
		inline float const getLinear() const{ return m_linear; };
		inline float const getQuadratic() const{ return m_quadratic; };

	private:
		float m_constant;
		float m_linear;
		float m_quadratic;

	};
}

#endif
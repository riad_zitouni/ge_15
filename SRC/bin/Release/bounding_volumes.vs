#version 330 core
layout (location = 0) in vec3 l_position;

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_modelMatrix;

void main()
{
	 gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vec4(l_position, 1.0f);
}
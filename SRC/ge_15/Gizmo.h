/*Class for transformation gizmos (translation, rotation, scaling, ....)*/

// Author: Riad Zitouni

#ifndef GIZMO_H
#define GIZMO_H

#include "Model.h"
#include "SceneShader.h"

#include <cfloat>

namespace ge15{
		
	enum GizmoMovement{
		GM_X_MOVE,
		GM_Y_MOVE,
		GM_Z_MOVE,
		
		GM_X_ROT,
		GM_Y_ROT,
		GM_Z_ROT,
		
		GM_X_SCALE,
		GM_Y_SCALE,
		GM_Z_SCALE,
		NONE
	};

	enum GizmoType{
		G_TRANSLATE,
		G_ROTATE,
		G_SCALE,
		G_NONE
	};

	class Gizmo{

	public:
		Gizmo();
		Gizmo(std::string modelDirectory);
		~Gizmo();
		
		inline std::vector<Model*> getModels() const { 
			return m_translationModels;
		}

		inline std::vector<Model*> getRotationModels() const {
			return m_rotationModels;
		}

		inline std::vector<Model*> getScaleModels() const {
			return m_scaleModels;
		}

		inline void setIsMoving(GizmoMovement direction){ m_moveDirection = direction; }
		inline GizmoMovement isMoving() const { return m_moveDirection; }

		void translate(glm::vec3 velocity, const float frameTime, std::vector<Drawable*> selectedObjects);
		void rotate(const float& angle, const glm::vec3 axis, std::vector<Drawable*> selectedObjects, const float frameTime);
		void scale(const glm::vec3 scale, std::vector<Drawable*> selectedObjects, const float frameTime);

		void centre(std::vector<Drawable*> selectedObjects);

		inline void setType(const GizmoType type){
			m_type = type;
		}

		inline const GizmoType getType() const{return m_type;}

		void clear();
		void clear(GizmoType type);

	private:
		Model* m_xAxis;
		Model* m_yAxis;
		Model* m_zAxis;

		Model* m_xRotation;
		Model* m_yRotation;
		Model* m_zRotation;

		Model* m_xScale;
		Model* m_yScale;
		Model* m_zScale;

		GizmoMovement m_moveDirection;

		std::vector<Model*> m_translationModels;
		std::vector<Model*> m_rotationModels;
		std::vector<Model*> m_scaleModels;

		GizmoType m_type;

		std::vector<Model*>& getGizmo(GizmoType type);

		SceneShader* shader = NULL;
	};
}

#endif
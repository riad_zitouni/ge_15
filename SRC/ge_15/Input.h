/*Class for registering keyboard input*/

// Author: Riad Zitouni

#ifndef INPUT_H
#define INPUT_H

namespace ge15{

#define MAX_KEYS 1024

	class Input{

	public:

		void press(unsigned int keyCode);
		void release(unsigned int keyCode);

		const bool isKeyPressed(unsigned int keyCode) const;

	private:
		bool m_keys[MAX_KEYS];
	};
}

#endif
// Author: Riad Zitouni

#include "IBO.h"


namespace ge15{

	IBO::IBO(){

	}
	
	IBO::IBO(GLuint* data, GLsizei size){

		m_count = size;

		glGenBuffers(1, &m_id);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(GLuint), data, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}

	IBO::~IBO(){
		glDeleteBuffers(1, &m_id);
	}

	void IBO::bind() const{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_id);
	}

	void IBO::unbind() const{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void IBO::setData(GLuint* data, GLsizei size){
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(GLuint), data, GL_STATIC_DRAW);
		m_count = size;
	}

}
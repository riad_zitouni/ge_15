// Author: Riad Zitouni

#include "MenuWindow.h"
#include "Logger.h"

namespace ge15{
	
	bool MenuWindow::m_isRunning;
	bool MenuWindow::m_exitButtonClicked;
	bool MenuWindow::m_openedFile;
	bool MenuWindow::m_addedPointLight;
	OPENFILENAME MenuWindow::m_openFileName;
	char* MenuWindow::m_selectedFileToOpen = "";
	int MenuWindow::m_textX = 10;
	int MenuWindow::m_textY = 10;
	
	MenuWindow::MenuWindow(){

	}

	MenuWindow::MenuWindow(HINSTANCE hInstance, int width, int height, const char* title){
		m_width = width;
		m_height = height;
		m_title = title;
		m_menuWindowClassName = "MenuWnd";
		m_openedFile = false;
		m_addedPointLight = false;
		Logger* logger = Logger::getInstance();
	}

	bool MenuWindow::init(HINSTANCE hInstance){
		Logger* logger = Logger::getInstance();
		logger->log("Please read README.txt for directions.");
		logger->log("Log:");
		m_textY += 40;

		m_menuWindowEx.cbSize = sizeof(WNDCLASSEX);
		m_menuWindowEx.style = CS_HREDRAW | CS_VREDRAW;
		m_menuWindowEx.lpfnWndProc = (WNDPROC)windowProc;
		m_menuWindowEx.cbClsExtra = 0;
		m_menuWindowEx.cbWndExtra = 0;
		m_menuWindowEx.hInstance = hInstance;
		m_menuWindowEx.hIcon = NULL;
		m_menuWindowEx.hCursor = LoadCursor(NULL, IDC_ARROW);
		m_menuWindowEx.hbrBackground = CreateSolidBrush(RGB(200, 200, 200));
		m_menuWindowEx.lpszMenuName = NULL;
		m_menuWindowEx.lpszClassName = m_menuWindowClassName;
		m_menuWindowEx.hIconSm = NULL;

		// Register class
		if (!RegisterClassEx(&m_menuWindowEx))
		{
			logger->log("ERROR::MENU_WINDOW::INIT::Failed to register the menu window class.");
			return false;
		}

		// Create the window
		m_menuWindowHandle = CreateWindowEx(WS_EX_APPWINDOW,
			m_menuWindowClassName,
			m_title,
			WS_OVERLAPPEDWINDOW | WS_VSCROLL, // WS_CAPTION makes window non-resizeable
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			m_width,
			m_height,
			NULL,
			NULL,
			hInstance,
			NULL);

		if (!(m_menuWindowHandle))
		{
			logger->log("ERROR::MENU_WINDOW::INIT::Failed to create the menu window.");
			return false;
		}

		// Show the window
		ShowWindow(m_menuWindowHandle, SW_SHOW);
		m_isRunning = true;
		m_exitButtonClicked = false;

		initOpenFileDialog();

		createMenuContent(m_menuWindowHandle);
		return true;
	}

	void MenuWindow::createMenuContent(const HWND hWindow){
		HMENU hMenu = CreateMenu();

		HMENU menu1 = addMenuItem(hMenu, "&File");
		HMENU menu2 = addMenuItem(hMenu, "&Light");

		addMenuSubItem(menu1, MenuActions::FILE_OPEN, "&Open");
		addMenuSubItem(menu1, MenuActions::FILE_EXIT, "&Exit");

		addMenuSubItem(menu2, MenuActions::ADD_POINT_LIGHT, "&Point Light");

		SetMenu(hWindow, hMenu);
	}

	HMENU MenuWindow::addMenuItem(HMENU hmenu, const char* name){
		HMENU hSubMenu = CreatePopupMenu();
		AppendMenu(hmenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, name);
		return hSubMenu;
	}

	void MenuWindow::addMenuSubItem(HMENU hmenu, MenuActions action, const char* name){
		AppendMenu(hmenu, MF_STRING, action, name);
	}

	void MenuWindow::initOpenFileDialog(){
		ZeroMemory(&m_openFileName, sizeof(m_openFileName));
		m_openFileName.lStructSize = sizeof (OPENFILENAME);
		m_openFileName.hwndOwner = NULL;
		m_openFileName.lpstrFilter = "All Files\0*.*\0\0";
		m_openFileName.nFilterIndex = 1;
		m_openFileName.lpstrFile = m_fileSize;
		m_openFileName.lpstrFile[0] = NULL;
		m_openFileName.nMaxFile = sizeof(m_fileSize);
		m_openFileName.lpstrFileTitle = NULL;
		m_openFileName.nMaxFileTitle = NULL;
		m_openFileName.lpstrInitialDir = NULL;
		m_openFileName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	}

	void MenuWindow::checkErrors(){
		if (PeekMessage(&m_message, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&m_message);
			DispatchMessage(&m_message);
		}
	}

	LRESULT MenuWindow::windowProc(HWND hWindow, UINT message, WPARAM wParam, LPARAM lParam)
	{
		HDC hdc;
		PAINTSTRUCT ps;
		Logger* logger = Logger::getInstance();
		switch (message)
		{
		case WM_PAINT:
		{
			hdc = BeginPaint(hWindow, &ps);
			for (int i = 0; i < logger->getLog().size(); i++){
				std::string log = logger->getLog()[i];
				TextOut(hdc, m_textX, m_textY, log.c_str(), strlen(log.c_str()));
				m_textY += 20;
			}
			m_textY = 10;
			EndPaint(hWindow, &ps);
			break;
		}
		case WM_CLOSE:
			DestroyWindow(hWindow);
			return 0;
		case WM_DESTROY:
			PostQuitMessage(0);
			m_isRunning = false;
			return 0;
		case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
			case MenuActions::FILE_OPEN:
				GetOpenFileName(&m_openFileName);
				m_selectedFileToOpen = m_openFileName.lpstrFile;
				m_openedFile = true;
				break;
			case MenuActions::FILE_EXIT:
				// Closes the application
				PostQuitMessage(0);
				m_isRunning = false;
				m_exitButtonClicked = true;
				break;
			case MenuActions::ADD_POINT_LIGHT:
				m_addedPointLight = true;
				break;
			}

		} break;
		default:
			return DefWindowProc(hWindow, message, wParam, lParam);
		}
	}

}
/*Class for gui panels*/

// Author: Riad Zitouni

#ifndef PANEL_H
#define PANEL_H

#include "Sprite.h"

namespace ge15{
	class Panel : public Sprite{
	public:
		Panel(const char* texturePath, const glm::vec3& position, const glm::vec3& size, Shader& guiShader, Material material = Material());
	};
}

#endif
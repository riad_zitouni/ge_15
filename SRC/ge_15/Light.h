/*Super class for all lights*/

// Author: Riad Zitouni

#ifndef LIGHT_H
#define LIGHT_H

#include "Model.h"
#include "Shader.h"

namespace ge15{

	class Light{

	public:
		Light();
		
		Light(const Shader& shader, glm::vec3 position, bool showModel = false, Material material =
			Material(glm::vec3(1.0f, 1.0f, 1.0f),
			glm::vec3(1.0f, 1.0f, 1.0f),
			glm::vec3(0.2f, 0.2f, 0.2f), 2.0f),
			std::string path = "Models/Samples/cube.obj");

		inline Drawable* getModel() const { return m_model; }
		const inline Material& getLightMaterial() const{ return m_lightMaterial; }

		inline void setPosition(const glm::vec3 position){ m_position = position; }
		inline void setModel(Drawable* model){ m_model = model; }

		inline const glm::vec3 getPosition() const{ return m_position; }

		inline void setIdPtr(int* ptr) { m_objectIdPtr = ptr; };

		~Light();

	protected:
		Drawable* m_model = NULL;
		Material m_lightMaterial;

	private:
		Shader m_shader;
		glm::vec3 m_position;

		bool m_update;

		int* m_objectIdPtr = NULL;

	};

}

#endif;
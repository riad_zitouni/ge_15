// Author: Riad Zitouni

#include "Drawable.h"

#include "MathUtils.h"
#include "ObjectManager.h"
#include "Window.h"
#include <glm/gtx/quaternion.hpp>


namespace ge15{

	Drawable::Drawable(glm::vec3 position, glm::vec3 scale, Material material, Label label){
		// Transformation initialization
		m_position = position;
		m_scale = scale;
		m_rotationAngle = 0;
		m_modelMatrix = glm::mat4(1.0);
		if (label != Label::MOVE_BUTTON && 
			label != Label::ROTATE_BUTTON &&
			label != Label::SCALE_BUTTON &&
			label != Label::X_AXIS &&
			label != Label::Y_AXIS &&
			label != Label::Z_AXIS &&
			label != Label::X_ROT &&
			label != Label::Y_ROT &&
			label != Label::Z_ROT &&
			label != Label::X_SCALE &&
			label != Label::Y_SCALE &&
			label != Label::Z_SCALE){
			m_modelMatrix = glm::translate(m_modelMatrix, m_position);
			m_modelMatrix = glm::rotate(m_modelMatrix, 0.0f, glm::vec3(1,0,0));
			m_modelMatrix = glm::scale(m_modelMatrix, m_scale);
		}

		m_rotationAxis = glm::vec3(1.0, 0.0, 0.0); // Keep this at 1 so objects don't disappear

		// Initialize material
		m_material = material;

		m_isSelected = false;
		m_isColliding = false;

		m_objectId = ObjectManager::getInstance()->generateId();

		m_label = label;
	}

	void Drawable::initTransformations(const glm::vec3& position, const glm::vec3& scale){
		m_position = position;
		m_scale = scale;
		m_modelMatrix = glm::mat4(1.0);
		m_modelMatrix = glm::translate(m_modelMatrix, m_position);
		m_modelMatrix = glm::scale(m_modelMatrix, m_scale);
	}

	void Drawable::setTranslation(const glm::vec3& velocity, const float& frameTime){
		m_position += (velocity * frameTime);
		update();
	}

	void Drawable::setRotation(const float& angle, const glm::vec3& axis, const float& frameTime){
		m_rotationAngle = (angle * frameTime);
		m_rotationAxis = axis;
		update();
	}

	void Drawable::update(){

		if (!m_isColliding){
			m_modelMatrix = glm::mat4();

			glm::mat4 translationMatrix(1.0);
			translationMatrix = glm::translate(translationMatrix, m_position);

			float x = m_rotationAxis.x * sin(m_rotationAngle / 2);
			float y = m_rotationAxis.y * sin(m_rotationAngle / 2);
			float z = m_rotationAxis.z * sin(m_rotationAngle / 2);
			float w = cos(m_rotationAngle / 2);

			glm::quat quaternion;
			quaternion = glm::quat(w, x, y, z);

			glm::mat4 currentRotation = glm::toMat4(quaternion);

			m_rotationMatrix = currentRotation * m_rotationMatrix;

			glm::mat4 scaleMatrix(1.0);
			scaleMatrix = glm::scale(scaleMatrix, m_scale);

			m_modelMatrix = translationMatrix * m_rotationMatrix * scaleMatrix;
		}
	}

	void Drawable::setScale(const glm::vec3& size, const float& frameTime){
		m_scale += (size * frameTime);
		update();
		m_boundingVolume.setSize(m_modelMatrix);
	}

	void Drawable::drawBoundingVolume(Shader& boundingVolumeShader, const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix){
		boundingVolumeShader.uniformMat4("u_projectionMatrix", projectionMatrix);
		boundingVolumeShader.uniformMat4("u_viewMatrix", viewMatrix);
		boundingVolumeShader.uniformMat4("u_modelMatrix", m_modelMatrix);
		m_boundingVolume.draw(); 
	}

	void Drawable::onMouseHover(double x, double y){
		if (isMouseOver(x, y)){
			m_isHovered = true;
		}
		else{
			m_isHovered = false;
		}
	}

	bool Drawable::isMouseOver(double x, double y) const{
		double mouseX = x / Window::getWidth();
		double mouseY = 1 - (y / Window::getHeight());

		mouseX = (2.0f * x) / Window::getWidth() - 1.0f;
		mouseY = 1.0f - (2.0f * y) / Window::getHeight();

		if ((mouseX > m_position.x && mouseX < m_position.x + m_scale.x) &&
			(mouseY > m_position.y && mouseY < m_position.y + m_scale.y)){
			return true;
		}

		return false;
	}

}
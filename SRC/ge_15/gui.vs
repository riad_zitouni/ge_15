#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 l_texCoord;

out vec2 texCoord;

uniform sampler2D u_textures;

void main()
{
	gl_Position = vec4(position.x, position.y, position.z, 1.0);
	texCoord = vec2(l_texCoord.x, 1.0 - l_texCoord.y);
}
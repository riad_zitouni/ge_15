/*Class for rendering window*/

// Author: Riad Zitouni

#ifndef WINDOW_H
#define WINDOW_H

#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace ge15{
	class Window{

	public:
		Window();
		Window(const char* title, int width, int height);
		~Window();

		bool init();
		void update();
		int close() const;
		void clearBuffers();

		inline const char* getTitle() const{ return m_title; }
		inline static const int getWidth() { return m_width; }
		inline static const int getHeight(){ return m_height; }
		inline const float getAspectRatio() const{ return ((float)m_width / m_height); }
		inline GLFWwindow* getWindow() const{ return m_window; }

		friend static void windowResizeCallback(GLFWwindow* window, int width, int height);

	private:
		GLFWwindow* m_window;
		static int m_width;
		static int m_height;
		const char* m_title;
	};
}

#endif;
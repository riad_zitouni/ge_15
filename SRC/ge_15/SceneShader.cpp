// Author: Riad Zitouni

#include "SceneShader.h"
#include <sstream>
#include "Logger.h"
namespace ge15{

	SceneShader::SceneShader(const char* vertexShaderPath, const char* fragmentShaderPath) : Shader(vertexShaderPath, fragmentShaderPath){
		for (int i = 0; i < MAX_POINT_LIGHTS; i++){
			m_pointLights[i] = NULL;
		}
	}

	SceneShader::~SceneShader(){
		for (int i = 0; i < MAX_POINT_LIGHTS; i++){
			if (m_pointLights[i] != NULL){
				delete m_pointLights[i];
			}
		}

		for (int i = 0; i < m_garbageLight.size(); i++){
			delete m_garbageLight[i];
		}
	}

	void SceneShader::setDirectionalLightProperties(const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular, const glm::vec3& direction){
		uniform3f("u_directionalLight.ambient", ambient);
		uniform3f("u_directionalLight.diffuse", diffuse);
		uniform3f("u_directionalLight.specular", specular);
		uniform3f("u_directionalLight.direction", direction);
	}

	bool SceneShader::addPointLight(PointLight* light){

		if (m_pointLightsSize >= MAX_POINT_LIGHTS){
			Logger* logger = Logger::getInstance();
			logger->log("Exceeded max number of point lights");
			return false;
		}

		m_pointLights[m_pointLightsSize] = light;

		setPointLightData(m_pointLightsSize, light);

		m_pointLightsSize++;
		uniform1f("u_activePointLights", (float)m_pointLightsSize);

		return true;
	}

	void SceneShader::removePointLight(const int objectId){

		int index = 0;
		for (int i = 0; i < m_pointLightsSize; i++){
			PointLight* current = m_pointLights[i];
			if (current->getModel()->getObjectId() == objectId){
				index = i;
				m_garbageLight.push_back(current);
				break;
			}
		}

		int offset = 0;
		for (int i = index + 1; i < MAX_POINT_LIGHTS; i++){
			m_pointLights[index + offset] = m_pointLights[i];
			m_pointLights[i] = NULL;
			offset++;
		}

		m_pointLightsSize--;
		
		for (int i = 0; i < m_pointLightsSize; i++){
			PointLight* light = m_pointLights[i];
			setPointLightData(i, light);
		}

		for (int i = m_pointLightsSize; i < MAX_POINT_LIGHTS; i++){
			setPointLightData(i, &PointLight());
		}

		uniform1f("u_activePointLights", (float)m_pointLightsSize);
	}

	void SceneShader::setPointLightData(int index, const PointLight* light){
		std::ostringstream ambientOss;
		std::ostringstream diffuseOss;
		std::ostringstream specularOss;
		std::ostringstream constantOss;
		std::ostringstream linearOss;
		std::ostringstream quadraticOss;

		ambientOss << "u_pointLights" << "[" << m_pointLightsSize << "]" << ".ambient";
		diffuseOss << "u_pointLights" << "[" << m_pointLightsSize << "]" << ".diffuse";
		specularOss << "u_pointLights" << "[" << m_pointLightsSize << "]" << ".specular";
		constantOss << "u_pointLights" << "[" << m_pointLightsSize << "]" << ".constant";
		linearOss << "u_pointLights" << "[" << m_pointLightsSize << "]" << ".linear";
		quadraticOss << "u_pointLights" << "[" << m_pointLightsSize << "]" << ".quadratic";

		std::string ambientString(ambientOss.str());
		std::string diffuseString(diffuseOss.str());
		std::string specularString(specularOss.str());
		std::string constantString(constantOss.str());
		std::string linearString(linearOss.str());
		std::string quadraticString(quadraticOss.str());

		uniform3f(ambientString.c_str(), light->getLightMaterial().ambient);

		uniform3f(diffuseString.c_str(), light->getLightMaterial().diffuse);
		uniform3f(specularString.c_str(), light->getLightMaterial().specular);

		uniform1f(constantString.c_str(), light->getConstant());
		uniform1f(linearString.c_str(), light->getLinear());
		uniform1f(quadraticString.c_str(), light->getQuadratic());
	}

	void SceneShader::updatePointLights(){
		for (int i = 0; i < m_pointLightsSize; i++){
			std::ostringstream positionOss;
			positionOss << "u_pointLights" << "[" << i << "]" << ".position";
			std::string ambientString(positionOss.str());
			uniform3f(ambientString.c_str(), m_pointLights[i]->getModel()->getPosition());
		}
	}

	void SceneShader::setViewMatrix(const glm::mat4& viewMatrix){
		uniformMat4("u_viewMatrix", viewMatrix);
	}

	void SceneShader::setViewMatrixPosition(const glm::vec3& position){
		uniform3f("u_viewPosition", position);
	}

	void SceneShader::setPerspectiveMatrixUniform(GLfloat fov, GLfloat aspectRatio, GLfloat near, GLfloat far){
		m_projectionMatrix = glm::perspective(fov, aspectRatio, near, far);
		uniformMat4("u_projectionMatrix", m_projectionMatrix);
	}
}
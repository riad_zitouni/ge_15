// Author: Riad Zitouni

#include "Sprite.h"
#include <SOIL.h>
#include "Logger.h"

namespace ge15{

	Sprite::Sprite(const char* texturePath, glm::vec3 position, glm::vec3 size, Material material, Label label, Shader& guiShader) : Drawable(position, size, material, label){
		init();
		m_textureID = loadTexture(texturePath);
		
		m_shader = guiShader;
	}
	Sprite::~Sprite(){
		glDeleteBuffers(1, &m_VBO);
		glDeleteBuffers(1, &m_VAO);
	}

	void Sprite::init(){
		float x0 = m_position.x;
		float y0 = m_position.y;
		float z = m_position.z;
		float width = m_scale.x;
		float height = m_scale.y;

		GLfloat vertices[] = {
			// Positions         // Colors
			x0, y0, 0.0f, 0.0f,// Bottom Left
			x0, y0 + height, 0.0f, 1.0f,// Top Left
			x0 + width, y0 + height, 1.0f, 1.0f,// Top Right

			x0 + width, y0 + height, 1.0f, 1.0f,// Top Right
			x0 + width, y0, 1.0f, 0.0f,// Bottom Right 
			x0, y0, 0.0f, 0.0f// Bottom Left
		};

		glGenVertexArrays(1, &m_VAO);
		glGenBuffers(1, &m_VBO);
		glBindVertexArray(m_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		// Position
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		// Tex coords
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);

		glBindVertexArray(0);
	}

	void Sprite::draw(){
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_textureID);

		m_shader.uniform1i("u_texture", 0);

		glBindVertexArray(m_VAO);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);
	}

	GLuint Sprite::loadTexture(const char* path){
		// Assign texture to ID
		GLuint texID;
		glGenTextures(1, &texID);
		glBindTexture(GL_TEXTURE_2D, texID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		int width, height;
		unsigned char* image = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGBA);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);

		SOIL_free_image_data(image);

		// Parameters
		glBindTexture(GL_TEXTURE_2D, 0);

		return texID;
	}

}
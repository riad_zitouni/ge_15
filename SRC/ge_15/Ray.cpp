// Author: Riad Zitouni

#include "Ray.h"
#include "Window.h"

namespace ge15{

	Ray::Ray(){

	}

	Ray::Ray(glm::vec3 origin, const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix, double mouseX, double mouseY){
		m_origin = origin;
		setDirectionFromMouse(mouseX, mouseY, projectionMatrix, viewMatrix);
	}

	void Ray::setDirectionFromMouse(double mouseX, double mouseY, const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix){
		float x = (2.0f * mouseX) / Window::getWidth() - 1.0f;
		float y = 1.0f - (2.0f * mouseY) / Window::getHeight();
		float z = 1.0f;

		glm::vec4 rayClip(x, y, -z, 1.0);

		// Convert to view coordinates
		glm::vec4 rayEye = glm::inverse(projectionMatrix) * rayClip;
		rayEye = glm::vec4(rayEye.x, rayEye.y, -1.0, 0.0);

		// Convert to world coordinates
		glm::vec4 rayWorld = glm::inverse(viewMatrix) * rayEye;

		m_direction = glm::vec3(rayWorld.x, rayWorld.y, rayWorld.z);
		m_direction = glm::normalize(m_direction);
	}

}
/*Class for first person camera*/

// Author: Riad Zitouni

#ifndef CAMERA_H
#define CAMERA_H

#include <glm\glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace ge15{

#define MAX_PITCH_ANGLE 89
	class Camera{
	
	public:
		enum CameraDirection{
			FORWARD,
			BACK,
			LEFT,
			RIGHT
		};

		Camera();
		~Camera();

		void update();

		void move(CameraDirection direction);
		void setPitchYaw(float x, float y);
		void setYaw(float angle);

		glm::mat4 getViewMatrix() const;

		inline const glm::vec3 getPosition() const{ return m_position; }

	private:

		float m_prevX;
		float m_prevY;

		float m_pitchAngle;
		float m_yawAngle;

		float m_moveSensitivity;
		float m_rotationSensitivity;

		glm::vec3 m_position;
		glm::vec3 m_lookAt;
		glm::vec3 m_direction;
		glm::vec3 m_up;
		glm::vec3 m_right;

		glm::quat m_rotation;

	};

}

#endif
// Author: Riad Zitouni

#include "Logger.h"

#include <ctime>
#include <sstream>

namespace ge15{

	Logger* Logger::m_instance = NULL;

	Logger::Logger(){
		m_update = false;
	}

	Logger::~Logger(){
		delete m_instance;
	}

	Logger* Logger::getInstance(){
		if (m_instance == NULL){
			m_instance = new Logger();
		}

		return m_instance;
	}

	void Logger::log(char* message){
		m_mutex.lock();

		makeSpace();
		m_log.push_back(std::string(message));
		m_update = true;

		m_mutex.unlock();

	}

	void Logger::log(const char* message){
		m_mutex.lock();

		makeSpace();
		m_log.push_back(std::string(message));
		m_update = true;
	
		m_mutex.unlock();

	}

	void Logger::log(const glm::vec3 vector){
		m_mutex.lock();

		makeSpace();

		std::string message;

		std::ostringstream stream;
		stream << "<" << vector.x << "," << vector.y << "," << vector.z << ">";

		message.append(stream.str());

		m_log.push_back(message);
		m_update = true;
		m_mutex.unlock();
	}

	void Logger::makeSpace(){
		if (m_log.size() >= MAX_LOG_SIZE){
			clear();
			m_log.push_back("Please read README.txt for directions.");
			m_log.push_back("Log:");
		}
	}

	void Logger::log(const char* part1,const char* part2){
		m_mutex.lock();

		makeSpace();
		// Get time and date
		time_t time = std::time(0);
		char* date = ctime(&time);

		std::string p1String = std::string(part1);
		std::string p2String = std::string(part2);

		std::string message;
		message.append("- ");
		message.append(date);
		message.append("- ");
		message.append(p1String);
		message.append(p2String);

		m_log.push_back(message);

		m_update = true;

		m_mutex.unlock();

	}

	void Logger::log(const char* part1, GLenum part2){
		m_mutex.lock();

		makeSpace();
		std::string p1String = std::string(part1);

		std::ostringstream stream;
		stream << part2;

		std::string message;
		message.append(p1String);
		message.append(stream.str());

		m_log.push_back(message);
		m_update = true;

		m_mutex.unlock();
	}

	void Logger::log(const char* part1, float part2){
		m_mutex.lock();

		makeSpace();
		std::string p1String = std::string(part1);
		
		std::ostringstream stream;  
		stream << part2;

		std::string message;
		message.append(p1String);
		message.append(stream.str());

		m_log.push_back(message);
		m_update = true;

		m_mutex.unlock();
	}

	void Logger::clear(){
		m_log.clear();
	}
}
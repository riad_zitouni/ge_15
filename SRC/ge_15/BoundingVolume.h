/*Class for object bounding volume used for collision detection*/

// Author: Riad Zitouni

#ifndef BOUNDING_VOLUME
#define BOUNDING_VOLUME

#include "Mesh.h"
#include "Ray.h"

namespace ge15{

	class BoundingVolume{

	public:
		BoundingVolume();
		BoundingVolume(std::vector<Vertex> vertices);

		float intersect(const Ray& ray, const glm::vec3& position, const glm::vec3& scale);
		void setSize(const glm::mat4& modelMatrix);

		inline const std::vector<glm::vec3> getVertices() const { return m_bvVertices; }
		inline const GLuint* getIndices() const { return m_bvIndex; }

		inline const glm::vec3& getSize() const { return m_size; }

		void init();

		void draw();

	private:
		bool isIntersectionInTriangle(const glm::vec3 intersectionPoint, glm::vec3& tp1, const glm::vec3& tp2, const glm::vec3& tp3);
		GLuint VBO, VAO, EBO;
		void createBoundingVolume(std::vector<Vertex> vertices);

		GLuint m_bvIndex[36]; 
		std::vector<glm::vec3> m_bvVertices;

		glm::vec3 m_position;

		glm::vec3 m_minVertex;
		glm::vec3 m_maxVertex;

		glm::vec3 m_size;
	};
}

#endif
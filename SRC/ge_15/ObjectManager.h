/*Class for managing all objects in the scene*/

// Author: Riad Zitouni

#ifndef OBJECT_MANAGER_H
#define OBJECT_MANAGER_H

#include "Drawable.h"

#include <vector>

namespace ge15{
	class ObjectManager{

	public:
		~ObjectManager();
		static ObjectManager* getInstance();

		inline std::vector<Drawable*>& getActiveObjects(){ return m_activeObjects; }
		inline std::vector<Drawable*>& getSelectedObjects(){ return m_selectedObjects; }

		inline void add(Drawable* drawable){
			if (!drawable->fileNotFound()){
				m_activeObjects.push_back(drawable);
			}
			else{
				delete drawable;
			}
		}

		inline void addSelectedObject(Drawable* drawable){
			m_selectedObjects.push_back(drawable);
		}

		inline int generateId(){
			++m_lastId;
			return m_lastId;
		}

		void deselectObject(const Drawable* drawable);

		void deselectAll();

	private:
		ObjectManager();

		static ObjectManager* m_instance;
		std::vector<Drawable*> m_activeObjects;
		std::vector<Drawable*> m_selectedObjects;

		int m_lastId;

	};
}

#endif
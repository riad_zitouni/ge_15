// Author: Riad Zitouni

#include "Light.h"

#include <iostream>
#include "Logger.h"

namespace ge15{

	Light::Light(){

	}

	Light::Light(const Shader& shader, glm::vec3 position, bool showModel, Material material, std::string path)
	{		
		Logger* logger = Logger::getInstance();
		if (showModel){
			m_model = new Model(
				path, position, glm::vec3(0.3f, 0.3f, 0.3f), 
				Material(glm::vec3(100.0f, 100.0f, 100.0f),
						 glm::vec3(1.0f, 1.0f, 1.0f),
						 glm::vec3(1.0f, 1.0f, 1.0f),
				2.0f), Label::LIGHT);
		}
		
		m_lightMaterial = material;
		m_shader = shader;
		m_position = m_model->getPosition();
		int id = m_model->getObjectId();
		m_objectIdPtr = &id;
	}
	
	Light::~Light(){
	}
}
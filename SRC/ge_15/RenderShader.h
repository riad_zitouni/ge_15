/*Shader class for rendering*/

// Author: Riad Zitouni

#ifndef RENDER_SHADER
#define RENDER_SHADER

#include "Shader.h"
#include "PointLight.h"
#include <vector>

namespace ge15{
	class RenderShader : public Shader{

		// Max number of point lights allowed in the scene 
#define MAX_POINT_LIGHTS 10

	public:


		RenderShader(const char* vertexShaderPath, const char* fragmentShaderPath);
		~RenderShader();

		void setDirectionalLightProperties(const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular, const glm::vec3& direction);

		bool addPointLight(PointLight* light);

		void removePointLight(const int objectId, bool indefiniteRemove);

		void updatePointLights();

		void setViewMatrix(const glm::mat4& viewMatrix);

		void setViewMatrixPosition(const glm::vec3& position);

		void setPerspectiveMatrixUniform(GLfloat fov, GLfloat aspectRatio, GLfloat near, GLfloat far);

		inline const glm::mat4 getProjectionMatrix() const{ return m_projectionMatrix; }
		inline PointLight** getPointLights() { return m_pointLights; }
		inline const int getPointLightsSize() const{ return m_pointLightsSize; }

	private:
		void setPointLightData(int index, const PointLight* light);

		glm::mat4 m_projectionMatrix;

		PointLight* m_pointLights[MAX_POINT_LIGHTS];
		std::vector<PointLight*> m_garbageLight;

		int m_pointLightsSize;
	};
}

#endif
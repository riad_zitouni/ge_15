/*Super class for any object that can be drawn on screen*/

// Author: Riad Zitouni

#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "Mesh.h"
#include "BoundingVolume.h"
#include "Shader.h"
#include <iostream>

namespace ge15{

	enum Label{
		// Light
		LIGHT, 
		// Translation Gizmo
		X_AXIS,
		Y_AXIS,
		Z_AXIS,
		// Rotation Gizmo
		X_ROT,
		Y_ROT,
		Z_ROT,
		// Scale Gizmo
		X_SCALE,
		Y_SCALE,
		Z_SCALE,
		// Buttons
		MOVE_BUTTON,
		ROTATE_BUTTON,
		SCALE_BUTTON,
		PLAY_BUTTON,
		// Other
		GUI,
		OBJECT,
	};

	struct Material{
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
		float shininess;

		Material(glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular, float shininess)
		{
			this->ambient = ambient;
			this->diffuse = diffuse;
			this->specular = specular;
			this->shininess = shininess;
		}

		Material(){
			ambient = glm::vec3(0.2f, 0.2f, 0.2f);
			diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
			specular = glm::vec3(0.1f, 0.1f, 0.1f);
			shininess = 2.0f;
		}
	};

	class Drawable{
	public:
		
		virtual ~Drawable(){
		
		}

		virtual void draw(){

		}

		virtual float intersect(const Ray& ray){
			return 0;
		}

		bool isMouseOver(double x, double y) const;
		void onMouseHover(double x, double y);

		void drawBoundingVolume(Shader& boundingVolumeShader, const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix);
		
		inline const glm::vec3& getPosition() const { return m_position; }

		inline const glm::vec3& getScale() const { return m_scale; }
		
		inline const glm::mat4& getModelMatrix() const { return m_modelMatrix; }
		inline const Material& getMaterial() const { return m_material; }
		
		inline const float getIsSelected() const { return (float)m_isSelected; }

		inline const int& getObjectId() const { return m_objectId; }

		inline const bool isHovered() const { return m_isHovered; }

		inline const GLsizei getIndex() const { return m_indexStart; }
		inline void setIndex(GLsizei index) { m_indexStart = index; }
		
		inline BoundingVolume getBoundingVolume() { return m_boundingVolume; }

		inline const Label& getLabel() const { return m_label; }

		inline void select(bool value) { m_isSelected = value; }
		inline void isColliding(bool value) { m_isColliding = value; }

		void initTransformations(const glm::vec3& position, const glm::vec3& scale);
		
		void setTranslation(const glm::vec3& velocity, const float& frameTime);
		void setRotation(const float& angles, const glm::vec3& axis, const float& frameTime);
		void setScale(const glm::vec3& scale, const float& frameTime);

		void update();

		inline const bool fileNotFound() const{ return m_fileNotFound; }

		std::vector<Mesh> meshes;
	protected:
		Drawable(){

		}

		Drawable(glm::vec3 position, glm::vec3 size, Material material, Label label = Label::OBJECT);

		glm::mat4 m_modelMatrix; 

		glm::vec3 m_position;
		
		glm::vec3 m_rotationAxis;
		float m_rotationAngle;
		glm::mat4 m_rotationMatrix;

		glm::vec3 m_scale;

		Material m_material;

		BoundingVolume m_boundingVolume;

		bool m_isSelected;

		int m_objectId;

		Label m_label;

		bool m_fileNotFound = false;

		private:
			
			bool m_isHovered;

			GLsizei m_indexStart;

			bool m_isColliding;

	};
}

#endif
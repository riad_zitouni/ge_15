// Author: Riad Zitouni

#include "Shader.h"
#include "FileLoader.h"

#include <vector>

#include <glm/gtc/type_ptr.hpp>

#include "Logger.h"


namespace ge15{

	Shader::Shader(){

	}

	Shader::Shader(const char* vertexShaderPath, const char* fragmentShaderPath){
		m_vertexShaderPath = vertexShaderPath;
		m_fragmentShaderPath = fragmentShaderPath;

		compileShader();

		use();
		
		// initialize texture id's 
		GLint texIDs[32];
		for (int i = 0; i < 32; i++){
			texIDs[i] = i;
		}
		uniform1iv("u_textures", 32, texIDs);

		unuse();
	}

	Shader::~Shader(){
		glDeleteProgram(m_program);
	}

	bool Shader::compileShader(){
		Logger* logger = Logger::getInstance();

		bool readResult = false;

		// Read shader files
		std::string vertexShaderString;
		std::string fragmentShaderString;
		
		readResult = readFile(m_vertexShaderPath, vertexShaderString);

		if (!readResult){
			return false;
		}

		readResult = readFile(m_fragmentShaderPath, fragmentShaderString);

		if (!readResult){
			return false;
		}

		// Generate shaders
		GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		if (vertexShader == 0){
			logger->log("ERROR::SHADER::COMPILE_SHADER::Failed to create vertex shader");
			return false;
		}
		else if (fragmentShader == 0){
			logger->log("ERROR::SHADER::COMPILE_SHADER::Failed to create fragment shader");
			return false;
		}

		// Compile vertex shader
		const char* vertexShaderSource = vertexShaderString.c_str();
		glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
		glCompileShader(vertexShader);

		GLint result;
		GLint bufferSize;
		GLchar log[512];
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &result);
		if (!result)
		{
			glGetShaderInfoLog(vertexShader, 512, NULL, log);
			logger->log("ERROR::SHADER::COMPILE_SHADER::Failed to compile vertex shader", log);

		}

		// Compile fragment shader
		const char* fragmentShaderSource = fragmentShaderString.c_str();
		glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
		glCompileShader(fragmentShader);

		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &result);
		if (!result)
		{
			glGetShaderInfoLog(fragmentShader, 512, NULL, log);
			logger->log("ERROR::SHADER::COMPILE_SHADER::Failed to compile fragment shader", log);
		}

		// Create program
		m_program = glCreateProgram();
		if (m_program == 0){
			logger->log("ERROR::SHADER::COMPILE_SHADER::Failed to create program");
			return false;
		}

		// Attach shaders
		glAttachShader(m_program, vertexShader);
		glAttachShader(m_program, fragmentShader);
		glLinkProgram(m_program);

		glGetProgramiv(m_program, GL_LINK_STATUS, &result);
		if (!result){
			glGetProgramInfoLog(m_program, 512, NULL, log);
			logger->log("ERROR::SHADER::COMPILE_SHADER::Failed to link program", log);
		}

		// Cleanup
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	void Shader::use(){
		glUseProgram(m_program);
	}

	void Shader::unuse(){
		glUseProgram(0);
	}

	void Shader::uniform1i(const GLchar* uniformName, const int value){
		glUniform1i(getUniform(uniformName), value);
	}

	void Shader::uniform1f(const GLchar* uniformName, const float value){
		glUniform1f(getUniform(uniformName), value);
	}

	void Shader::uniform2f(const GLchar* uniformName, const glm::vec2& value){
		glUniform2f(getUniform(uniformName), value.x, value.y);
	}

	void Shader::uniform3f(const GLchar* uniformName, const glm::vec3& value){
		glUniform3f(getUniform(uniformName), value.x, value.y, value.z);
	}

	void Shader::uniform4f(const GLchar* uniformName, const glm::vec4& value){
		glUniform4f(getUniform(uniformName), value.x, value.y, value.z, value.w);
	}

	void Shader::uniform1iv(const GLchar* uniformName, int size, int* value){
		glUniform1iv(getUniform(uniformName), size, value);
	}

	void Shader::uniformMat4(const GLchar* uniformName, const glm::mat4& value){
		glUniformMatrix4fv(getUniform(uniformName), 1, GL_FALSE, glm::value_ptr(value));
	}

	GLuint Shader::getUniform(const char* uniformName){
		return glGetUniformLocation(m_program, uniformName);
	}


}